/*
===============================================================================
 Name        : test_PCM1802_dac.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "board.h"
#include "FreeRTOS.h"
#include "task.h"

#include "i2s_17xx_40xx.h"

/*****************************************************************************
 * Private defines
 ****************************************************************************/

#define LED_stick			0,22
#define LED_on				1
#define LED_off				0

#define LEN					1

#define I2SRX_CLK			0,4
#define I2SRX_WS			0,5
#define I2SRX_SDA			0,6
#define RX_MCLK				4,28

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

static volatile uint8_t channelTC, channelTC1, dmaChannelNum_adc, dmaChannelNum_dac;
uint32_t DMAbuffer[LEN], DMAbuffer1;

/* Sets up system hardware */
static void InitHardware(void)
{
	SystemCoreClockUpdate();

	// Led del stick P0,22
	Chip_IOCON_PinMux(LPC_IOCON, LED_stick, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_stick);

	// Init DAC
	Chip_DAC_Init(LPC_DAC);

	// Init I2S
	Chip_IOCON_PinMux(LPC_IOCON, I2SRX_CLK, MD_PLN, IOCON_FUNC1);
	Chip_IOCON_PinMux(LPC_IOCON, I2SRX_SDA, MD_PLN, IOCON_FUNC1);
	Chip_IOCON_PinMux(LPC_IOCON, I2SRX_WS, MD_PLN, IOCON_FUNC1);
	Chip_IOCON_PinMux(LPC_IOCON, RX_MCLK, MD_PLN, IOCON_FUNC1);

	I2S_AUDIO_FORMAT_T audio_Confg;
	audio_Confg.SampleRate = 48000;
	/* Select audio data is 2 channels (1 is mono, 2 is stereo) */
	audio_Confg.ChannelNumber = 2;
	/* Select audio data is 16 bits */
	audio_Confg.WordWidth = 16;

	Chip_I2S_Init(LPC_I2S);
	Chip_I2S_RxModeConfig(LPC_I2S, 0, I2S_TXMODE_4PIN_ENABLE, ENABLE);
	Chip_I2S_SetRxBitRate(LPC_I2S, 2);
	Chip_I2S_SetRxXYDivider(LPC_I2S, 3, 2);

	Chip_I2S_RxConfig(LPC_I2S, &audio_Confg);
	Chip_I2S_Int_RxCmd(LPC_I2S, ENABLE, 1);

	NVIC_EnableIRQ(I2S_IRQn);

	Chip_I2S_RxStop(LPC_I2S);
	Chip_I2S_RxStart(LPC_I2S);
}

static int32_t data;
static uint16_t adcFlag = 0;

void I2S_IRQHandler(void)
{
	if(Chip_I2S_GetRxLevel(LPC_I2S))
	{
		data = Chip_I2S_Receive(LPC_I2S);
		adcFlag=1;
	}
}

/*****************************************************************************
 * Public functions
 ****************************************************************************/

int main(void)
{

	InitHardware();

	while(1){

		if(adcFlag)
		{
			Chip_DAC_UpdateValue(LPC_DAC, data);
			adcFlag = 0;
		}

	}

	/* Should never arrive here */
	return 1;
}

