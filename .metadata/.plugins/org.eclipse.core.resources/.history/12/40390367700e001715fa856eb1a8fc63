/*
====================================================================================================================
 Name        : pwm.c
 Author      : Agustin Díaz Antuña
 Contacto	 : * audiodplab@frba.utn.edu.ar
 	 	 	   * Asunto: Workspace TD2 LPC1769
 Version     : 1.0
 Copyright   : $(copyright)
 Date		 : 02/08/16
 Description : Proyecto realizado para probar el periferico PWM.
 	 	 	   Utilizando las interrupciones de PWM, se hace togglear un pin a partir de una funcion que indica durante
 	 	 	   cuanto tiempo estara habilitado y a que frecuencia.
 Board		 : --
 Peripherals : GPIO (led) - PWM (IRQ)
 FreeRTOS	 : SI (queues)
====================================================================================================================
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define LPC_PWM1 				  ((LPC_PWM_T 			   *) LPC_PWM1_BASE)

#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "chip_lpc175x_6x.h"
#include "pwm_17xx_40xx.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// LEDs
#define LED_STICK		0,22
#define LED_ON			1
#define LED_OFF			0

// PWM
#define PWM_pin			4,29


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Hardware init
void prvSetupHardware(void)
{
	Chip_SetupXtalClocking();
	Chip_SYSCTL_SetFLASHAccess(FLASHTIM_100MHZ_CPU);

	SystemCoreClockUpdate();

	// LEDS
	Chip_IOCON_PinMux(LPC_IOCON, LED_STICK, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_STICK);

	// Estado inicial
	Chip_GPIO_SetPinState(LPC_GPIO, LED_STICK, LED_OFF);
}

void InitPWM0(void)
{
	//Initialize PWM peipheral, timer mode
	//-----------------------------------------------------------------------------------------------
	Chip_PWM_PrescaleSet(LPC_PWM1, 0);		//Valor preescalar=100 (divisor de clock)

	//Set match value for PWM match channel0 (frecuency)
	//-----------------------------------------------------------------------------------------------
	Chip_PWM_SetMatch(LPC_PWM1, 0, 29275);		//Establezco el valor en clock del período (canal 0) 25kHz
	Chip_PWM_MatchEnableInt(LPC_PWM1, 0);		//Habilito interrupción
	Chip_PWM_ResetOnMatchEnable(LPC_PWM1, 0);	//Reset auto
	Chip_PWM_StopOnMatchDisable(LPC_PWM1, 0);	//No stop

	//Reset and Start Counter
	//-----------------------------------------------------------------------------------------------
	Chip_PWM_Reset(LPC_PWM1);

	//Start PWM
	//-----------------------------------------------------------------------------------------------
	Chip_PWM_Enable(LPC_PWM1);

	//Enable PWM interrupt
	//-----------------------------------------------------------------------------------------------
	NVIC_EnableIRQ(PWM1_IRQn);

	Chip_IOCON_PinMux(LPC_IOCON, PWM_pin, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, PWM_pin);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Interrupciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//INTERRUPT PWM
void PWM1_IRQHandler(void)
{
	//Interupción Canal 0 -> PERIODO
	//-----------------------------------------------------------------------------------------------
	if (Chip_PWM_MatchPending(LPC_PWM1, 0))	//Reviso interrupción pendiente canal PWM 0
	{
		Chip_PWM_ClearMatch(LPC_PWM1, 0);	//Limpio interrupción canal PWM 0

		Chip_GPIO_SetPinToggle(LPC_GPIO, PWM_pin);
	}
}

void tono(int time_ms, int frecuencia)
{
	Chip_PWM_SetMatch(LPC_PWM1, 0, frecuencia);		//Establezco el valor en clock del período (canal 0) 25kHz
	Chip_PWM_MatchEnableInt(LPC_PWM1, 0);			//Habilito interrupción
	vTaskDelay( time_ms / portTICK_RATE_MS );
	Chip_PWM_MatchDisableInt(LPC_PWM1, 0);			//Habilito interrupción
}


////////////////////////////////////////////////////////////////////////////
// Tasks
////////////////////////////////////////////////////////////////////////////

// Blinky led stick
void vLEDTask(void *pvParameters)
{
	while (1)
	{
		vTaskDelay( 1000 / portTICK_RATE_MS );
		Chip_GPIO_SetPinToggle(LPC_GPIO, LED_STICK);
	}
}

// Blinky led stick
void vPWMTask(void *pvParameters)
{
	while (1)
	{
		tono(3000, 10000);				// 3 seg a 10 KHz
		tono(3000, 20000);				// 3 seg a 20 KHz
		tono(3000, 30000);				// 3 seg a 30 KHz
	}
}

////////////////////////////////////////////////////////////////////////////
// MAIN
////////////////////////////////////////////////////////////////////////////

int main(void)
{
	prvSetupHardware();
	InitPWM0();

	xTaskCreate(vLEDTask, (signed char *) "vLEDTask",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate(vPWMTask, (signed char *) "vPWMTask",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	//Start the scheduler
	vTaskStartScheduler();

	//Should never arrive here
	return 1;
}

