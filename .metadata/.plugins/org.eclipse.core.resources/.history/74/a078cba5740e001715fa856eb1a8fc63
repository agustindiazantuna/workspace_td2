/*
====================================================================================================================
 Name        : parcial1.c
 Author      : Agustin Díaz Antuña
 Contacto	 : * audiodplab@frba.utn.edu.ar
 	 	 	   * Asunto: Workspace TD2 LPC1769
 Version     : 1.0
 Copyright   : $(copyright)
 Date		 : 02/08/16
 Description : Proyecto realizado para probar el periferico PWM.
 	 	 	   Utilizando las interrupciones de PWM, se hace togglear un pin a partir de una funcion que indica durante
 	 	 	   cuanto tiempo estara habilitado y a que frecuencia.
 	 	 	   Hay problemas con el .h y .c correspondiente a pwm que esta incluido en la libreria chip, por lo tanto
 	 	 	   tuve que agregar los correspondientes archivos encontrados en internet.
 Board		 : --
 Peripherals : GPIO (led) - PWM (IRQ)
 FreeRTOS	 : SI (queues)
====================================================================================================================
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "chip.h"
#include "board.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Led
#define LEDStick		0,22
#define LED_on			1
#define LED_off			0

// Switch
#define SW1 2,10		// Botón Comienzo/Detención
#define SW2 0,18		// Botón cuenta ascendente (ingresó una persona)
#define SW3 1,13		// Botón cuenta descendente (egresó una persona)

// Debounce
#define WAIT_1 0
#define VALIDATE_1 1
#define WAIT_0 2
#define VALIDATE_0 3

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Declaro las colas a utilizar
xQueueHandle CuentaQueue;
xQueueHandle TeclaQueue;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Tasks
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Tarea propia encargada de realizar el antirrebote de los botones.
static void vDEBOUNCETask(void *pvParameters) {

	int EstadoDebounce = WAIT_1, tecla_actual = 0, tecla_anterior = 0;

	while (1)
	{
		switch(EstadoDebounce)
		{
		case WAIT_1:
			if( Chip_GPIO_GetPinState( LPC_GPIO , SW1 ) == 0 )
					tecla_actual=1;
			if( Chip_GPIO_GetPinState( LPC_GPIO , SW2 ) == 0 )
					tecla_actual=2;
			if( Chip_GPIO_GetPinState( LPC_GPIO , SW3 ) == 0 )
					tecla_actual=3;
			if( tecla_actual != 0)
				EstadoDebounce = VALIDATE_1;
			tecla_anterior = tecla_actual;
			break;
		case VALIDATE_1:
			vTaskDelay(50/portTICK_RATE_MS);

			if( Chip_GPIO_GetPinState( LPC_GPIO , SW1 ) == 0 )
					tecla_actual=1;
			if( Chip_GPIO_GetPinState( LPC_GPIO , SW2 ) == 0 )
					tecla_actual=2;
			if( Chip_GPIO_GetPinState( LPC_GPIO , SW3 ) == 0 )
					tecla_actual=3;
			if( tecla_anterior == tecla_actual)
			{
				EstadoDebounce = WAIT_0;
				xQueueSendToBack( TeclaQueue, &tecla_actual, portMAX_DELAY );
			}
			else
				EstadoDebounce = WAIT_1;
			tecla_actual = 0;
			break;
		case WAIT_0:
			if( Chip_GPIO_GetPinState( LPC_GPIO , SW1 ) == 0 )
					tecla_actual=1;
			if( Chip_GPIO_GetPinState( LPC_GPIO , SW2 ) == 0 )
					tecla_actual=2;
			if( Chip_GPIO_GetPinState( LPC_GPIO , SW3 ) == 0 )
					tecla_actual=3;
			if( tecla_actual == 0 )
				EstadoDebounce = VALIDATE_0;
			tecla_actual = 0;
			break;
		case VALIDATE_0:
			vTaskDelay(50/portTICK_RATE_MS);

			if( Chip_GPIO_GetPinState( LPC_GPIO , SW1 ) == 0 )
					tecla_actual=1;
			if( Chip_GPIO_GetPinState( LPC_GPIO , SW2 ) == 0 )
					tecla_actual=2;
			if( Chip_GPIO_GetPinState( LPC_GPIO , SW3 ) == 0 )
					tecla_actual=3;
			if( 0 == tecla_actual)
				EstadoDebounce = WAIT_1;
			else
				EstadoDebounce = WAIT_0;
			tecla_actual = 0;
			break;
		default:
			break;
		}
	}
}

// Tarea que monitorea los sensores de presencia (ascendente como descendente) y lleva la cuenta correspondiente.
void CuentaTask(void *pvParameters)
{
	int Cantidad_Personas = 0, buffer_tecla = 0, tecla = 0, conteo = 0;

	while (1)
	{
		xQueuePeek(TeclaQueue, &buffer_tecla, portMAX_DELAY);						// Miro la cola si el botón apretado es Comienzo/Detención
		if ( buffer_tecla == 1 && conteo == 0 )										// Comienza el conteo
			conteo = 1;
		else if ( ( buffer_tecla == 2 || buffer_tecla == 3 ) && conteo == 1 )		// TeclaQueue vale 1 cuando se apretó el botón Comienzo/Detención
		{
			if (  xQueueReceive(TeclaQueue, &tecla, portMAX_DELAY) == 1 )			// Leo tecla recibida que guardó en una cola la tarea DEBOUNCE
			{
				if ( tecla == 2 && Cantidad_Personas < 10 )							// Ingresó una persona
					Cantidad_Personas ++;
				else if ( tecla == 3 && Cantidad_Personas > 0 )						// Se fue una persona
					Cantidad_Personas --;

				xQueueSendToBack(CuentaQueue, &Cantidad_Personas, portMAX_DELAY);	// Escribo el la cola las variaciones de Cantidad_Personas
			}
		}
		else if ( buffer_tecla == 1 && conteo == 1 )								// Termina el conteo y debo resetear el contador
		{
			conteo = 0;
			Cantidad_Personas = 0;
		}

		buffer_tecla = 0;
	}
}

// Tarea de control que monitorea al pulsador Comienzo/Detención y se encarga del manejo de leds.
void ControlTask(void *pvParameters)
{
	int buffer_tecla = 0, conteo = 0, Cantidad_Personas = 0, i = 0;

	while (1)
	{
		xQueuePeek(TeclaQueue, &buffer_tecla, portMAX_DELAY);						// Veo que tecla se oprimió

		if ( buffer_tecla == 1 )													// TeclaQueue vale 1 cuando se apretó el botón Comienzo/Detención
		{
			xQueueReceive(TeclaQueue, &buffer_tecla, portMAX_DELAY);				// Leo cola del DEBOUNCER
			if ( conteo == 0 )
			{
				conteo = 1;															// Indica si está realizándose o no el conteo. Inicia.
				Chip_GPIO_SetPinToggle(LPC_GPIO, LEDStick);							// LED comienza apagado, se prende, testigo "cuenta en curso"
			}
			else
			{
				conteo = 2;															// Finaliza conteo.
				Chip_GPIO_SetPinToggle(LPC_GPIO, LEDStick);							// Se apaga el led, testigo "cuenta en curso"
			}
		}

		if ( conteo == 1 )															// Comenzó el conteo
			xQueueReceive(CuentaQueue, &Cantidad_Personas, portMAX_DELAY);			// Leo cola de la entrada/salida de personas
		else if ( conteo == 2 )														// Finalizó el conteo debo hacer parpadear el led
		{
			Cantidad_Personas = Cantidad_Personas*2;								// Multiplico por dos para que al togglear 2 veces parpadee el led
			for ( i = 0 ; i < Cantidad_Personas ; i++ )								// Toggleo el led con la cantidad indicada en Cantidad_Personas
			{
				vTaskDelay( 1000 / portTICK_RATE_MS );								// Parpadea cada 1 segundo.
				Chip_GPIO_SetPinToggle(LPC_GPIO, LEDStick);
			}
			Cantidad_Personas = 0;
			conteo = 0;																// Indico que finalizó el estado de Comienzo/Detención, reseteo variables.
		}

		buffer_tecla = 0;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Functions
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void prvSetupHardware(void)
{
	SystemCoreClockUpdate();

	// Inicialización de la placa LPC Board
    Board_Init();

    // Set the LED to the state of "On"
    Board_LED_Set(0, true);

	// Led del stick P0,22
	Chip_IOCON_PinMux(LPC_IOCON, LEDStick, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LEDStick);

	// Estado inicial del LED, apagado.
	Chip_GPIO_SetPinState(LPC_GPIO, LEDStick, LED_off);

	// Inicialización de pulsadores
	Chip_IOCON_PinMux(LPC_IOCON, SW1, MD_PUP, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, SW1);

	Chip_IOCON_PinMux(LPC_IOCON, SW2, MD_PUP, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, SW2);

	Chip_IOCON_PinMux(LPC_IOCON, SW3, MD_PUP, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, SW3);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void)
{
	// Variables
	CuentaQueue = xQueueCreate(1,sizeof(int));		// Creo cola la variación de Cantidad_Personas
	TeclaQueue = xQueueCreate(1,sizeof(int));		// Creo cola para comunicar la tecla leída con el DEBOUNCER

	// Inicialización
	prvSetupHardware();

	// Tareas
	xTaskCreate(vDEBOUNCETask, "vDEBOUNCETask",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate(CuentaTask, "CuentaTask",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate(ControlTask, "ControlTask",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}

