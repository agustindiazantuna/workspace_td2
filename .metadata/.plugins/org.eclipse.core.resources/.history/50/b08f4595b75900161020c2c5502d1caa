
////////////////////////////////////////////////////////////////////////////
// Includes
////////////////////////////////////////////////////////////////////////////

#include "board.h"
#include "FreeRTOS.h"
#include "task.h"

#include "semphr.h"
#include "queue.h"

#include "chip_lpc175x_6x.h"
#include "pwm_17xx_40xx.h"

////////////////////////////////////////////////////////////////////////////
// Defines
////////////////////////////////////////////////////////////////////////////

#define LED_port		0
#define LED_pin			22
#define LED_on			1
#define LED_off			0

#define PWM_port		4
#define PWM_pin			29

#define blu_port		2
#define blu_pin			1
#define red_port		2
#define red_pin			2
#define gre_port		2
#define gre_pin			3

#define TIEMPO			100

#define red				0
#define gre				1
#define yell			2
#define blu				3

#define		START		0
#define		NEXT		1
#define		WAIT		2

#define time 100

#define		SALIDA		1
#define		bien		1
#define		mal			2
////////////////////////////////////////////////////////////////////////////
// Variables
////////////////////////////////////////////////////////////////////////////

xSemaphoreHandle SEM_led;
xQueueHandle queue_gano;
xQueueHandle queue_perdio;
xQueueHandle queue_secuencia;
xQueueHandle queue_teclaok;
xQueueHandle queue_primertecla;

volatile int estado = 0;
volatile int random[16];

////////////////////////////////////////////////////////////////////////////
// Funciones
////////////////////////////////////////////////////////////////////////////

// Hardware init
static void prvSetupHardware(void)
{
	Chip_SetupXtalClocking();
	Chip_SYSCTL_SetFLASHAccess(FLASHTIM_100MHZ_CPU);

	SystemCoreClockUpdate();

	// Led del stick P0,22
	Chip_IOCON_PinMux(LPC_IOCON, LED_port, LED_pin, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_port, LED_pin);
}

void InitPWM0(void)
{
	//Initialize PWM peipheral, timer mode
	//-----------------------------------------------------------------------------------------------
	Chip_PWM_PrescaleSet(LPC_PWM1, 0);		//Valor preescalar=100 (divisor de clock)

	//Set match value for PWM match channel0 (frecuency)
	//-----------------------------------------------------------------------------------------------
	Chip_PWM_SetMatch(LPC_PWM1, 0, 39683);		//Establezco el valor en clock del período (canal 0) 25kHz
//	Chip_PWM_MatchEnableInt(LPC_PWM1, 0);		//Habilito interrupción
	Chip_PWM_MatchDisableInt(LPC_PWM1, 0);
	Chip_PWM_ResetOnMatchEnable(LPC_PWM1, 0);	//Reset auto
	Chip_PWM_StopOnMatchDisable(LPC_PWM1, 0);	//No stop

	//Reset and Start Counter
	//-----------------------------------------------------------------------------------------------
	Chip_PWM_Reset(LPC_PWM1);

	//Start PWM
	//-----------------------------------------------------------------------------------------------
	Chip_PWM_Enable(LPC_PWM1);

	//Enable PWM interrupt
	//-----------------------------------------------------------------------------------------------
	NVIC_EnableIRQ(PWM1_IRQn);

	//Pin PWM
	Chip_IOCON_PinMux(LPC_IOCON, PWM_port, PWM_pin, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, PWM_port, PWM_pin);

}

//INTERRUPT PWM
void PWM1_IRQHandler(void)
{
	//Interupción Canal 0 -> PERIODO
	//-----------------------------------------------------------------------------------------------
	if (Chip_PWM_MatchPending(LPC_PWM1, 0))	//Reviso interrupción pendiente canal PWM 0
	{
		Chip_PWM_ClearMatch(LPC_PWM1, 0);	//Limpio interrupción canal PWM 0

		if(estado == 1)
		{
			Chip_GPIO_WriteDirBit(LPC_GPIO,4,29,1);
			estado = 0;
		}
		else
		{
			Chip_GPIO_WriteDirBit(LPC_GPIO,4,29,0);
			estado = 1;
		}

	}
}

void parpadear(int port, int pin, int time_ms, int cicles)
{
	int i = 0;
	for(i = 0; i<cicles ; i++)
	{
		Chip_GPIO_SetPinToggle(LPC_GPIO, port, pin);
		vTaskDelay( time_ms / portTICK_RATE_MS );
	}
}

void tono(int time_ms, int frecuencia)
{
	Chip_PWM_SetMatch(LPC_PWM1, 0, frecuencia);		//Establezco el valor en clock del período (canal 0) 25kHz
	Chip_PWM_MatchEnableInt(LPC_PWM1, 0);			//Habilito interrupción
	vTaskDelay( time_ms / portTICK_RATE_MS );
	Chip_PWM_MatchDisableInt(LPC_PWM1, 0);			//Habilito interrupción
}

////////////////////////////////////////////////////////////////////////////
// Tasks
////////////////////////////////////////////////////////////////////////////

/* LED1 toggle thread */
static void vLEDTask1(void *pvParameters) {

	// Estado inicial
	Chip_GPIO_SetPinState(LPC_GPIO, LED_port, LED_pin, 0);

	while (1)
	{
		vTaskDelay( 1000 / portTICK_RATE_MS );
		Chip_GPIO_SetPinToggle(LPC_GPIO, LED_port, LED_pin);
	}
}


static void vSecuencia(void *pvParameters) {

	int secuencia_actual = 1 , contador = 0 , estado_secuencia = START, teclaok = 0;

	while (1)
	{
		switch(estado_secuencia)
		{
		case(START):
				if( contador < secuencia_actual && xQueueReceive(queue_primertecla, &secuencia_actual, portMAX_DELAY) == 1 )
					estado_secuencia = NEXT;
		case(NEXT):
				if( contador < secuencia_actual )
				{
					contador ++;
					xQueueSendToBack(queue_secuencia, &contador, portMAX_DELAY);
				}
				if( contador == secuencia_actual )
				{
					estado_secuencia = WAIT;

				}
		case(WAIT):
				if( xQueueReceive(queue_teclaok, &teclaok, portMAX_DELAY) == 1 )
				{
					contador = 0;
					if( teclaok == bien && secuencia_actual == 16 )
					{
						estado_secuencia = START;
						secuencia_actual = 1;
						xQueueSendToBack(queue_gano,&secuencia_actual,portMAX_DELAY);
					}
					else if( teclaok == mal )
					{
						estado_secuencia = START;
						xQueueSendToBack(queue_perdio,&secuencia_actual,portMAX_DELAY);
						secuencia_actual = 1;
					}
					else
					{
						secuencia_actual ++;
						estado_secuencia = NEXT;
					}
				}
		}
	}
}


static void vGanamos(void *pvParameters) {

	int queue = 0, cont = 3;

	while (1)
	{
		if( xQueueReceive(queue_gano, &queue, portMAX_DELAY) == 1 )
		{
			if( cont )
			{
				parpadear(red_port, red_pin, TIEMPO, 1);
				parpadear(red_port,red_pin,50,1);

				parpadear(blu_port, blu_pin, TIEMPO, 1);
				parpadear(blu_port, blu_pin, 50, 1);

				parpadear(gre_port, gre_pin, TIEMPO, 1);
				parpadear(gre_port, gre_pin, 50, 1);


				cont--;
			}
			cont = 3;

			parpadear(red_port, red_pin, TIEMPO, 1);


		}
	}
}

static void vPerdiste(void *pvParameters) {

	int secuencia = 0, cont = 0;
	while (1)
	{
		if( xQueueReceive(queue_perdio, &secuencia, portMAX_DELAY) == 1 )
		{
			for( cont=0 ; cont<secuencia ; cont++ )
			{
				switch(random[cont])
				{
				case red:
					parpadear(red_port, red_pin, time, 1);
					//tono(time, red_f);
				case yell:
					//parpadear(yell_port, yell_pin, time, 1);
					//tono(time, yell_f);
				case blu:
					parpadear(blu_port, blu_pin, time, 1);
					//tono(time, blu_f);
				case gre:
					parpadear(gre_port, gre_pin, time, 1);
					//tono(time, gre_f);
				}
			}
		}
	}
}

static void vShow(void *pvParameters) {

	int secuencia = 0, time_sec=0;
	while (1)
	{
		if( xQueueReceive(queue_secuencia, &secuencia, portMAX_DELAY) == 1 )
		{
			if( secuencia < 6 )
				time_sec = 400;
			else if( secuencia < 14 )
				time_sec = 300;
			else
				time_sec = 200;

			switch(random[secuencia])
			{
				case red:
					parpadear(red_port, red_pin, time_sec, 1);
					//tono(time, red_f);
				case yell:
					//parpadear(yell_port, yell_pin, time_sec, 1);
					//tono(time, yell_f);
				case blu:
					parpadear(blu_port, blu_pin, time_sec, 1);
					//tono(time, blu_f);
				case gre:
					parpadear(gre_port, gre_pin, time_sec, 1);
					//tono(time, gre_f);
			}
			vTaskDelay( 50 / portTICK_RATE_MS );

		}
	}
}

//static void vCheck(void *pvParameters) {
//int secuencia=0;
//
//
//	while (1)
//	{
//		if( xQueueReceive(queue_secuencia, &secuencia, portMAX_DELAY) == 1 )
//		{
//			if( secuencia < 6 )
//				time = 400;
//			else if( secuencia < 14 )
//				time = 300;
//			else
//				time = 200;
//
//			switch(random[secuencia])
//			{
//				case red:
//					parpadear(red_port, red_pin, time, 1);
//					//tono(time, red_f);
//				case yell:
//					parpadear(yell_port, yell_pin, time, 1);
//					//tono(time, yell_f);
//				case blu:
//					parpadear(blu_port, blu_pin, time, 1);
//					//tono(time, blu_f);
//				case gre:
//					parpadear(gre_port, gre_pin, time, 1);
//					//tono(time, gre_f);
//			}
//			vTaskDelay( 50 / portTICK_RATE_MS );
//
//		}
//
//	}
//}

////////////////////////////////////////////////////////////////////////////
// MAIN
////////////////////////////////////////////////////////////////////////////

typedef struct tecla{
	int codigo;
	int tiempo;
}TECLA;
#define BOTON_NO_PRESIONADO 44
#define DEBOUNCE 14
#define NO_OPRIMIDO 45
#define OPRIMIDO 46
#define BOTON_PRESIONADO 47
#define VALIDAR 48

#define SW1 2,10
#define SW4 0,18
#define SW7 0,11
#define SW10 2,13
#define SW13 1,26

static void vCheck ( void )
{
		unsigned int EstadoDebounce = NO_OPRIMIDO;
		TECLA tecla;
		portTickType xMeDesperte;
		//Inicio de variables y recuperación de parámetros
		xMeDesperte = xTaskGetTickCount();
		tecla.codigo= BOTON_NO_PRESIONADO;
		tecla.tiempo= 0;
		static int tiempo_de_pulsacion=0;
		static int tecla_actual=0 , tecla_anterior =0;

		while
			(1){
			//debo verificar rebote
			switch(EstadoDebounce)
			{
			case NO_OPRIMIDO:
				vTaskDelayUntil(&xMeDesperte,100/portTICK_RATE_MS);

				if( Chip_GPIO_GetPinState( LPC_GPIO , SW1 ) )
						tecla_actual=1;
				if( Chip_GPIO_GetPinState( LPC_GPIO , SW4 ) )
						tecla_actual=2;
				if( Chip_GPIO_GetPinState( LPC_GPIO , SW7 ) )
						tecla_actual=3;
				if( Chip_GPIO_GetPinState( LPC_GPIO , SW10 ) )
						tecla_actual=4;
				if( Chip_GPIO_GetPinState( LPC_GPIO , SW13 ) )
						tecla_actual=5;
				if( tecla_actual != 0)
					EstadoDebounce = DEBOUNCE;    //Indico que esta corriendo el tiempo Debounce
				break;
			case DEBOUNCE:
				vTaskDelay(50/portTICK_RATE_MS);
				EstadoDebounce = VALIDAR;
			case VALIDAR:
				if( tecla_anterior == tecla_actual)
					//Si retorna algo sigue presionado
				{
					EstadoDebounce=OPRIMIDO;
					tiempo_de_pulsacion = 0;
				}
				else
					// fue error
					EstadoDebounce=NO_OPRIMIDO;
				break;
			case OPRIMIDO:
					tecla.codigo= tecla_actual;
					tecla.tiempo= tiempo_de_pulsacion;
					tiempo_de_pulsacion = -1;
					//Envío a la cola, optamos por no bloquear si está llena
					xQueueSend( xtecla, &tecla, 0 );
					EstadoDebounce = NO_OPRIMIDO;
				break;
			default:
				break;
			}
		}
	}


int main(void)
{

	// Creamos el semáforo.
	vSemaphoreCreateBinary(SEM_led);

	queue_gano = xQueueCreate(1, sizeof(int));
	queue_perdio = xQueueCreate(1, sizeof(int));
	queue_secuencia = xQueueCreate(1, sizeof(int));
	queue_teclaok = xQueueCreate(1, sizeof(int));
	queue_primertecla = xQueueCreate(1, sizeof(int));


	prvSetupHardware();
	InitPWM0();

	xTaskCreate(vLEDTask1, (signed char *) "vTaskLed1",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate(vSecuencia, (signed char *) "vSecuencia",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate(vGanamos, (signed char *) "vGanamos",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate(vPerdiste, (signed char *) "vPerdiste",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate(vShow, (signed char *) "vShow",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	//xTaskCreate(vCheck, (signed char *) "vCheck",
		//		configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
			//	(xTaskHandle *) NULL);

	//Start the scheduler
	vTaskStartScheduler();

	//Should never arrive here
	return 1;
}

