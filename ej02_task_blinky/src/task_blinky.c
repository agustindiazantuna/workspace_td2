/*
====================================================================================================================
 Name        : task_blinky.c
 Author      : Agustin Díaz Antuña
 Contacto	 : * audiodplab@frba.utn.edu.ar
 	 	 	   * Asunto: Workspace TD2 LPC1769
 Version     : 1.0
 Copyright   : $(copyright)
 Date		 : 02/08/16
 Description : Practica realizada en clase.
 	 	 	   Una tarea le avisa a la otra mediante una queue la cantidad de
 	 	 	   veces que debe parpadear un led.
 Board		 : --
 Peripherals : GPIO (led)
 FreeRTOS	 : SI (queues)
====================================================================================================================
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "chip.h"
#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include <cr_section_macros.h>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define LED_port		0
#define LED_pin			22
#define LED_on			1
#define LED_off			0

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

xQueueHandle cola;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Tareas
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Tarea que le envia a traves de una queue la cantidad de veces que debe parpadear el led
void vTarea1(void *pvParameters)
{
	int item = 5;												// Cantidad de veces que el led tiene que parpadear
	Chip_GPIO_SetPinState(LPC_GPIO, LED_port, LED_pin, 0);
	xQueueSendToBack(cola, &item, portMAX_DELAY);

	while(1)
	{
		vTaskDelay(portMAX_DELAY);								// Para que el scheduler no le asigne tiempo a una tarea que no hace nada
		// También se podría matar la taera con
		// vTaskDelete(NULL);
		// Si la función vTaskDelete recibe un puntero a NULL hace referencia a si misma y se autodestruye
		// Esto podria realizarse desde la tarea2 colocando el puntero a esta tarea (la tarea1) para lo cual en la creacion de la misma
		// se le deberia asignar una variable global a modo de puntero o pasar como parametro.
	}
}

// Tarea que saca el valor de la queue y blinkea la cantidad de veces indicada
void vTarea2(void *pvParameters)
{
	int recibido = 0;
	xQueueReceive(cola, &recibido, portMAX_DELAY);				// Leo la queue una sola vez
	recibido *= 2;												// Multiplico para que parpadee

	while(1)
	{
		if(recibido != 0)
		{
			Chip_GPIO_SetPinToggle(LPC_GPIO, LED_port, LED_pin);
			vTaskDelay( 1000 / portTICK_RATE_MS );
			recibido --;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Inicializacion
void prvSetupHardware(void)
{
	SystemCoreClockUpdate();

	// Configuramos los pines para todos los puntos
	// Led del stick P0,22
	Chip_IOCON_PinMux(LPC_IOCON, LED_port, LED_pin, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_port, LED_pin);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void)
{
	// Creo cola
	cola = xQueueCreate(1, sizeof(int));

	// Inicializo
	prvSetupHardware();

    // Creo tareas
	xTaskCreate( vTarea1, "vTarea1",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate( vTarea2, "vTarea2",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}
