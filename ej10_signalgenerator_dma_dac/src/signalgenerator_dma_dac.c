/*
====================================================================================================================
 Name        : signalgenerator_dma_dac.c
 Author      : Agustin Díaz Antuña en base a ejemplo de Juan Bacigalupo
 Contacto	 : * audiodplab@frba.utn.edu.ar
 	 	 	   * Asunto: Workspace TD2 LPC1769
 Version     : 1.0
 Copyright   : $(copyright)
 Date		 :
 Description : Practica realizada en clase a partir del ejemplo dado por el ayudante de cátedra
 	 	 	   Generador de señales precario.
 	 	 	   Cada cierto tiempo (4 segundos aprox) cambia la salida siguiendo esta secuencia:
 	 	 	   							SENO -> TRIANGULAR -> RECTANGULAR
 	 	 	   A partir de una tabla, utilizando el DMA, muestra la misma por el DAC del LPC.
 	 	 	   Tiene realizada una modificación en la función Chip_GPDMA_SGTransfer por eso se realizó una propia
 	 	 	   función llamada my_Chip_GPDMA_SGTransfer().
 Board		 : --
 Peripherals : GPIO (led) - DMA - DAC
 FreeRTOS	 : NO
====================================================================================================================
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "chip.h"
#include "gpio_17xx_40xx.h"
#include "iocon_17xx_40xx.h"
#include "gpdma_17xx_40xx.h"
#include <cr_section_macros.h>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Define
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define LED_STICK			0,22

#define PUERTO(X)			 X
#define PIN(X)				 X

#define NUMERO_MUESTRAS 	60
#define DMA_SIZE			60
#define FRECUENCIA_SENOIDAL	60

#define CLOCK_DAC_HZ		30000000	// CCLK divido por 4. NOTA: Clock que trae por defecto con ésta configuración: 120MHz.

#define PRIMER_CUADRANTE 	15
#define SEGUNDO_CUADRANTE 	30
#define TERCER_CUADRANTE 	45
#define CUARTO_CUADRANTE 	60

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

typedef enum {SENO, TRIANGULAR, RECTANGULAR} salida;
salida ESTADO = SENO;

DMA_TransferDescriptor_t DMA_LLI_Struct;

uint16_t seno_0_a_90 [ 16 ] = {  0  , 1045 , 2079 , 3090 , 4067 , 5000 , 5877 , 6691 , 7431 , 8090 , \
			                   8660 , 9135 , 9510 , 9781 , 9945 , 10000 };

uint16_t TABLA_DAC [ NUMERO_MUESTRAS ];

uint32_t Canal_Libre;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Inicializacion
void Configurar_uC (void)
{
	uint32_t Tiempo;

	/* Configuracion de los GPIO necesarios */

	Chip_GPIO_Init (LPC_GPIO);	// Por compatibilidad. No tiene sentido práctico ya que el módulo comienza energizado.
	Chip_IOCON_PinMux (LPC_IOCON , PUERTO(0) , PIN(26) , MD_PLN, IOCON_FUNC2 );

	Chip_IOCON_PinMux(LPC_IOCON, LED_STICK, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_STICK);

	/* Configuracion del DAC */

	Chip_DAC_Init(LPC_DAC);

	Tiempo = CLOCK_DAC_HZ / ( FRECUENCIA_SENOIDAL * NUMERO_MUESTRAS ); // Fija la separacion entre muestras.

	Chip_DAC_SetDMATimeOut(LPC_DAC, Tiempo);						   // Se configura el TimeOut.

	Chip_DAC_ConfigDAConverterControl(LPC_DAC, DAC_DBLBUF_ENA | DAC_CNT_ENA | DAC_DMA_ENA); // Se habilita el DMA y soporte de cuenta.

	/* Configuracion del DMA */

	Chip_GPDMA_Init ( LPC_GPDMA );

	Chip_GPDMA_PrepareDescriptor ( LPC_GPDMA , &DMA_LLI_Struct , (uint32_t) TABLA_DAC ,
								   GPDMA_CONN_DAC , DMA_SIZE , GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA , &DMA_LLI_Struct );

	Canal_Libre = Chip_GPDMA_GetFreeChannel ( LPC_GPDMA , 0 );
}

// Funcion encargada de llenar la tabla que el DMA saca por el DAC
uint8_t llenar_tabla (salida ESTADO)
{
	uint8_t i = 0;

	switch(ESTADO)
	{
	case SENO:
		for ( i = 0 ; i < NUMERO_MUESTRAS ; i++ )
		{
			if ( i <= PRIMER_CUADRANTE )		// De 0 a 90º
			{
				TABLA_DAC[i] = 512 + 512 * seno_0_a_90 [i] / 10000;
				   if ( i == PRIMER_CUADRANTE )
					  TABLA_DAC[i]= 1023;
			}
			else
				if ( i <= SEGUNDO_CUADRANTE )	// De 90º a 180º
	 			   TABLA_DAC[i] = 512 + 512 * seno_0_a_90[ SEGUNDO_CUADRANTE - i ] / 10000;

			    else
				   if( i <= TERCER_CUADRANTE )	// De 180º a 270º
	  			      TABLA_DAC[i] = 512 - 512 * seno_0_a_90[ i - SEGUNDO_CUADRANTE ] / 10000;

				   else							// De 270º a 360º
					  TABLA_DAC[i] = 512 - 512 * seno_0_a_90[ CUARTO_CUADRANTE - i ] / 10000;

			TABLA_DAC [i] = ( TABLA_DAC [i] << 6); // Se escala. Bits [5:0] del reg DACR son reservados.
		}
		break;
	case TRIANGULAR:
		for(i=0;i<60;i++)
		{
			if(i < 30)
				TABLA_DAC[i] = i*34;
			else
				TABLA_DAC[i] = (60-i)*34;

			TABLA_DAC[i] = ( TABLA_DAC [i] << 6); // Se escala. Bits [5:0] del reg DACR son reservados.
		}
		break;
	case RECTANGULAR:
		for(i=0;i<60;i++)
		{
			if(i < 30)
				TABLA_DAC[i] = 1023;
			else
				TABLA_DAC[i] = 0;

			TABLA_DAC[i] = ( TABLA_DAC [i] << 6); // Se escala. Bits [5:0] del reg DACR son reservados.
		}
		break;
	default:
		return 0;
		break;
	}
	return 1;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones privadas
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* Control which set of peripherals is connected to the DMA controller */
STATIC uint8_t configDMAMux(uint32_t gpdma_peripheral_connection_number)
{
#if defined(CHIP_LPC175X_6X)
	if (gpdma_peripheral_connection_number > 15) {
		LPC_SYSCTL->DMAREQSEL |= (1 << (gpdma_peripheral_connection_number - 16));
		return gpdma_peripheral_connection_number - 8;
	}
	else {
		if (gpdma_peripheral_connection_number > 7) {
			LPC_SYSCTL->DMAREQSEL &= ~(1 << (gpdma_peripheral_connection_number - 8));
		}
		return gpdma_peripheral_connection_number;
	}
#elif defined(CHIP_LPC177X_8X) || defined(CHIP_LPC40XX)
	if (gpdma_peripheral_connection_number > 15) {
		LPC_SYSCTL->DMAREQSEL |= (1 << (gpdma_peripheral_connection_number - 16));
		return gpdma_peripheral_connection_number - 16;
	}
	else {
		LPC_SYSCTL->DMAREQSEL &= ~(1 << (gpdma_peripheral_connection_number));
		return gpdma_peripheral_connection_number;
	}
#endif
}

// Funcion Chip_GPDMA_SGTransfer modificada ya que la propia de LPCOpen tiene problemas
/* Do a DMA scatter-gather transfer M2M, M2P,P2M or P2P using DMA descriptors */
Status my_Chip_GPDMA_SGTransfer(LPC_GPDMA_T *pGPDMA,
							 uint8_t ChannelNum,
							 const DMA_TransferDescriptor_t *DMADescriptor,
							 GPDMA_FLOW_CONTROL_T TransferType)
{
	const DMA_TransferDescriptor_t *dsc = DMADescriptor;
	GPDMA_CH_CFG_T GPDMACfg;
	uint8_t SrcPeripheral = 0, DstPeripheral = 0;
	uint32_t src = DMADescriptor->src, dst = DMADescriptor->dst;

	dst = (uint32_t) GPDMA_CONN_DAC; // Se debe forzar ésta condición. Caso contrario no funcionaría porque
									 // la función Chip_GPDMA_InitChannegCfg acepta un índice de periferico
									 // y no la posición absoluta.

	int ret;

	ret = Chip_GPDMA_InitChannelCfg(pGPDMA, &GPDMACfg, ChannelNum, src, dst, 0, TransferType);
	if (ret < 0) {
		return ERROR;
	}

	/* Adjust src/dst index if they are memory */
	if (ret & 1) {
		src = 0;
	}
	else {
		SrcPeripheral = configDMAMux(src);
	}

	if (ret & 2) {
		dst = 0;
	}
	else {
		DstPeripheral = configDMAMux(dst);
	}

	if (setupChannel(pGPDMA, &GPDMACfg, dsc->ctrl, dsc->lli, SrcPeripheral, DstPeripheral) == ERROR) {
		return ERROR;
	}

	/* Start the Channel */
	Chip_GPDMA_ChannelCmd(pGPDMA, ChannelNum, ENABLE);
	return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void)
{
	uint32_t i = 0;

    SystemCoreClockUpdate();

    Configurar_uC ();

    if(llenar_tabla(SENO) != 1)
    	return 0;

    my_Chip_GPDMA_SGTransfer (LPC_GPDMA , Canal_Libre , &DMA_LLI_Struct , GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA);

	i = 0;

	while(1)
	{
		i++;						// Delay muy precario
		if(i == 40000000)
		{
			Chip_GPIO_SetPinToggle(LPC_GPIO, LED_STICK);

			Chip_GPDMA_Stop(LPC_GPDMA, Canal_Libre);

			switch(ESTADO)
			{
			case SENO:
			    if(llenar_tabla(TRIANGULAR) != 1)
			    	return 0;
			    ESTADO = TRIANGULAR;
			    break;
			case TRIANGULAR:
			    if(llenar_tabla(RECTANGULAR) != 1)
			    	return 0;
			    ESTADO = RECTANGULAR;
			    break;
			case RECTANGULAR:
			    if(llenar_tabla(SENO) != 1)
			    	return 0;
			    ESTADO = SENO;
			    break;
			default:
				return 0;
				break;
			}
			i = 0;
			my_Chip_GPDMA_SGTransfer (LPC_GPDMA , Canal_Libre , &DMA_LLI_Struct , GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA);
		}
	}

    return 0 ;
}
