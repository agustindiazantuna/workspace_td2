/*
===============================================================================
 Name        : test_PCM1802_dac.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "board.h"
#include "FreeRTOS.h"
#include "task.h"

#include "i2s_17xx_40xx.h"

/*****************************************************************************
 * Private defines
 ****************************************************************************/

#define LED_stick			0,22
#define LED_on				1
#define LED_off				0

#define I2SRX_CLK			0,4
#define I2SRX_WS			0,5
#define I2SRX_SDA			0,6
#define RX_MCLK				4,28
#define I2STX_CLK			0,7
#define I2STX_WS			0,8
#define I2STX_SDA			0,9
//#define I2STX_SDA			2,13
#define TX_MCLK				4,29

#define SYNC				0,10

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

// PA USAR DMA


static uint8_t send_flag;
static uint8_t channelTC = 0;
static uint8_t dmaChannelNum_I2S_Rx;
static uint8_t dma_send_receive = 1;
static uint32_t DMAbuffer[100];



/*****************************************************************************
 * Private functions
 ****************************************************************************/

/* Sets up system hardware */
static void InitHardware(void)
{
	// Configuro el timer del micro en 120MHz						NO ANDA!
	Chip_SetupXtalClocking();
	Chip_SYSCTL_SetFLASHAccess(FLASHTIM_120MHZ_CPU);

	SystemCoreClockUpdate();

	// Led del stick P0,22
	Chip_IOCON_PinMux(LPC_IOCON, LED_stick, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_stick);

	// SYNC
	Chip_IOCON_PinMux(LPC_IOCON, SYNC, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, SYNC);

	// Init DAC
	Chip_DAC_Init(LPC_DAC);

	// Init I2S
	// Configuro los pines de RX de P0, ver defines
	Chip_IOCON_PinMux(LPC_IOCON, I2SRX_CLK, MD_PLN, IOCON_FUNC1);
	Chip_IOCON_PinMux(LPC_IOCON, I2SRX_SDA, MD_PLN, IOCON_FUNC1);
	Chip_IOCON_PinMux(LPC_IOCON, I2SRX_WS, MD_PLN, IOCON_FUNC1);
	Chip_IOCON_PinMux(LPC_IOCON, RX_MCLK, MD_PLN, IOCON_FUNC1);
	// Configuro los pibes del TX de P0, ver defines
	Chip_IOCON_PinMux(LPC_IOCON, I2STX_CLK, MD_PLN, IOCON_FUNC1);

//	Chip_IOCON_PinMux(LPC_IOCON, I2STX_SDA, MD_PLN, IOCON_FUNC0);
//	Chip_GPIO_SetPinDIR(LPC_GPIO, I2STX_SDA, 1);
//	Chip_GPIO_SetPinOutHigh(LPC_GPIO, I2STX_SDA);
//	Chip_GPIO_SetPinOutLow(LPC_GPIO, I2STX_SDA);

	Chip_IOCON_PinMux(LPC_IOCON, I2STX_WS, MD_PLN, IOCON_FUNC1);
	Chip_IOCON_PinMux(LPC_IOCON, TX_MCLK, MD_PLN, IOCON_FUNC1);

	// Configuro I2S_TX usando la estructura I2S_AUDIO_FORMAT_T y modifico la función
	I2S_AUDIO_FORMAT_T audio_Confg;
	audio_Confg.SampleRate = 32000;
	/* Select audio data is 2 channels (1 is mono, 2 is stereo) */
	audio_Confg.ChannelNumber = 2;
	/* Select audio data is 16 bits */
	audio_Confg.WordWidth = 32;

	// Configuro el clk del periférico para que trabaje a 96MHz
	Chip_Clock_SetPCLKDiv(SYSCTL_PCLK_I2S, SYSCTL_CLKDIV_1);

	// Inicializo el periférico I2S
	Chip_I2S_Init(LPC_I2S);
	Chip_I2S_RxStop(LPC_I2S);
	Chip_I2S_TxStop(LPC_I2S);
	Chip_I2S_DisableMute(LPC_I2S);

//	Chip_I2S_RxModeConfig(LPC_I2S, 0, 1, ENABLE);
//	Chip_I2S_SetRxBitRate(LPC_I2S, 1);
//	Chip_I2S_SetRxXYDivider(LPC_I2S, 1, 1);
	Chip_I2S_RxConfig(LPC_I2S, &audio_Confg);

				// Deshabilito int para probar DMA
				Chip_I2S_Int_RxCmd(LPC_I2S, ENABLE, 1);
//				Chip_I2S_Int_RxCmd(LPC_I2S, DISABLE, 1);

//	Chip_I2S_TxModeConfig(LPC_I2S, 0, 0, 1);
//	Chip_I2S_SetTxBitRate(LPC_I2S, 5);
//	Chip_I2S_SetTxXYDivider(LPC_I2S, 32, 125);
	Chip_I2S_TxConfig(LPC_I2S, &audio_Confg);
//	Chip_I2S_Int_TxCmd(LPC_I2S, ENABLE, 1);






	// CON I2S RX AL DMA Y AL DAC

//	/* Initialize GPDMA controller */
//	Chip_GPDMA_Init(LPC_GPDMA);
//
//	//  Chip_I2S_DMA_TxCmd(LPC_I2S, I2S_DMA_REQUEST_CHANNEL_1, ENABLE, 4);
//	Chip_I2S_DMA_RxCmd(LPC_I2S, I2S_DMA_REQUEST_CHANNEL_1, ENABLE, 1);
//
//	dmaChannelNum_I2S_Rx = Chip_GPDMA_GetFreeChannel(LPC_GPDMA, GPDMA_CONN_I2S_Channel_0);
//
//	Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNum_I2S_Rx,
//		GPDMA_CONN_I2S_Channel_0,
//		(uint32_t) &DMAbuffer,
//		GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA,
//		100);



// no sirve parece
//	Chip_GPDMA_ChannelCmd(LPC_GPDMA, dmaChannelNum_I2S_Rx, ENABLE);




	// Arranca I2S
	Chip_I2S_RxStart(LPC_I2S);
	Chip_I2S_TxStart(LPC_I2S);

				// Deshabilito int para probar DMA
				NVIC_EnableIRQ(I2S_IRQn);
//				NVIC_DisableIRQ(I2S_IRQn);

//				NVIC_EnableIRQ(DMA_IRQn);

}

static int32_t data, dataa = 0xFAFAF0F0;
//static uint16_t adcFlag = 0;
static uint16_t adcFlag = 1;

void I2S_IRQHandler(void)
{
	if(Chip_I2S_GetRxLevel(LPC_I2S))
	{
		data = Chip_I2S_Receive(LPC_I2S);
		adcFlag=1;
		send_flag = 1;
	}
}














 void DMA_IRQHandler(void)
 {
   if (Chip_GPDMA_Interrupt(LPC_GPDMA, dmaChannelNum_I2S_Rx) == SUCCESS) {
    channelTC = 1;
    Chip_GPDMA_ClearIntPending(LPC_GPDMA, GPDMA_STATCLR_INTTC, dmaChannelNum_I2S_Rx);
    Chip_GPDMA_ClearIntPending(LPC_GPDMA, GPDMA_STATCLR_INTERR, dmaChannelNum_I2S_Rx);
   }
 }













/*****************************************************************************
 * Public functions
 ****************************************************************************/

int main(void)
{
	int count = 0, i = 0, sel = 0, ii = 0;

	i=0;

	InitHardware();

	while(1){

		// I2S tx por polling cuando interrumpe rx
		if (Chip_I2S_GetRxLevel(LPC_I2S) < 1 && send_flag == 1) {
			Chip_I2S_Send(LPC_I2S, dataa);
			send_flag = 0;
		}

		if(adcFlag)
		{
			if(data < 0)
				data = (data >> 8);
			else
			{
				data = data >> 8 ;
				data &= (0x00FFFFFF);
			}

			if(data < 1024)
				data = data + 512;
			else
				data = data - 512;



//			// Funciona Rx con DAC del LPC
//			if(channelTC == 1)
//			{
//				for(i=0;i<100;i++)
//				{
//					if(DMAbuffer[i] < 0)
//						DMAbuffer[i] = (DMAbuffer[i]);
//					else
//					{
//						DMAbuffer[i] = DMAbuffer[i] >> 8 ;
//						DMAbuffer[i] &= (0x00FFFFFF);
//					}
//
//					if(DMAbuffer[i] < 1024)
//						DMAbuffer[i] = DMAbuffer[i] + 512;
//					else
//						DMAbuffer[i] = DMAbuffer[i] - 512;
//
//					Chip_DAC_UpdateValue(LPC_DAC, DMAbuffer[i]);
//					channelTC = 0;
//				}
//
//				Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNum_I2S_Rx,
//					GPDMA_CONN_I2S_Channel_0,
//					(uint32_t) &DMAbuffer,
//					GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA,
//					100);
//
//			}

			Chip_DAC_UpdateValue(LPC_DAC, data);

			adcFlag = 0;
			sel = 1;

		}

		if(count == 500000)
		{
			Chip_GPIO_SetPinToggle(LPC_GPIO, LED_stick);
			count = 0;
		}
		count ++;
	}

	/* Should never arrive here */
	return 1;
}

