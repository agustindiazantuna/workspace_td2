# Workspace LPC1769 #

Este workspace fue utilizado durante el cursado de la materia Técnicas Digitales 2 (TD2) en la Universidad Tecnológica Nacional (UTN) Facultad Regional Buenos Aires (FRBA) año 2016.

Contiene las prácticas realizadas en clase sobre el MCU LPC1769 de NXP basados en arquitectura ARM Cortex-M3.

Los proyectos sirven a modo de ejemplos sencillos del uso de ciertos periféricos utilizando la librería LPCOpen V2.10 y el sistema operativo en tiempo real FreeRTOS V8.2.3


### Contacto ###

Si considerás que se pueden agregar más proyectos, o encontrás errores en los mismos avisame.

* audiolab@frba.utn.edu.ar
* agustin.diazantuna@gmail.com
* Asunto: Workspace TD2 LPC1769


### Lista de proyectos ###

* my_blinky
* task_blinky
* my_freertos_blinky
* pwm_timer
* buttoncounter_lpcboard
* buttoncounter_infotronic
* adc_led
* pwm
* eeprom_lpc1769
* signalgenerator_dma_dac
* adc_dac_loopback
* i2s_mixto
* i2s_conversores_externos
* test_dsp_pridolfi
* parcial1
* parcial2


===============================================================================
#### my_blinky ####
Practica realizada en clase. Primer acercamiento a LPCOpen. Blinky del stick

* Board		 : --
* Peripherals	 : GPIO (led)
* FreeRTOS	 : NO

===============================================================================
#### task_blinky #####
Practica realizada en clase. Una tarea le avisa a la otra mediante una queue la cantidad de veces que debe parpadear un led.

* Board		 : --
* Peripherals 	 : GPIO (led)
* FreeRTOS	 : SI (queues)

===============================================================================
#### my_freertos_blinky ####
Practica realizada en clase.

Punto A: Parpadea cada 1 segundo dentro de una tarea usando vTaskDelay. Colocar el "#define PUNTO_A" en 1 y el resto en 0.

Punto A1: Parpadea a otra frecuencia utilizando funciones para setear un 1 o un 0 en vez de utilizar la función toggle. Colocar el "#define  PUNTO_A1" en 1 y el resto en 0.

Punto B: Queremos que parpadee a una frecuencia de 1uS. Pero no se puede hacer con el vTaskDelay ya que su base de tiempo es mayor. Entonces vamos a usar un timer y en su interrupción hacemos que el led togglee. Tiene otro tiempo de match comentado para que sea visual, cambiar si así lo desea. Colocar el "#define PUNTO_B" en 1 y el resto en 0.

Punto B1: Idem al punto B. Solo que ahora queremos que parpadee durante 5 segundos y durante otros 5 segundos no lo haga. Usamos vTaskDelay para ello. Colocar el "#define PUNTO_B1" en 1 y el resto en 0.

Punto C: Hace lo mismo que el punto B, pero ahora el toggleo lo hacemos en la tarea. En la interrupción, a traves de un semaforo, le avisamos a la tarea que el tiempo del match ha transcurrido. Colocar el "#define PUNTO_C" en 1 y el resto en 0.

* Board		 : --
* Peripherals	 : GPIO (led) - TIMER (IRQ)
* FreeRTOS	 : SI (semaphore )

===============================================================================
#### pwm_timer ####
Practica realizada en clase.

Punto 1: Usando el timer 0 y dos niveles de match, se hace parpadear al led en base a la frecuencia elegida, toggleando el estado del led en cada IRQ. No usa FreeRTOS. Colocar el "#define PUNTO_1" en 1 y el resto en 0.

Punto 2: Usando el timer 0 y dos niveles de match, cuando interrupe por cada match se entrega un semaforo para que una tarea prenda y otra apague el led segun que match provoco la IRQ. Colocar el "#define PUNTO_2" en 1 y el resto en 0.

Punto 3: Hace lo mismo que el punto 2, agregando una tarea que hace parpadear el led del stick a otra frecuencia. Colocar el "#define PUNTO_3" en 1 y el resto en 0.

* Board		 : --
* Peripherals	 : GPIO (led) - TIMER (IRQ)
* FreeRTOS	 : SI (semaphore)

===============================================================================
#### buttoncounter_lpcboard ####
Practica realizada en clase. Al presionar el boton CTRL comenzamos la cuenta, con el botón IN aumentamos el numero contado. Al volver a apretar el boton CTRL un led parpadea el valor almacenado.

* Board		 : LPC 1769 BOARD
* Peripherals	 : GPIO (botones, led)
* FreeRTOS	 : SI (queues)

===============================================================================
#### buttoncounter_infotronic ####
Practica realizada en clase. Al presionar el boton SW1 comenzamos la cuenta, con el botón SW2 aumentamos el numero contado. Al volver a apretar el boton SW1 un led parpadea el valor almacenado. Incluye antirrebote clasico.

* Board		 : Infotronic V2.0
* Peripherals	 : GPIO (button - led)
* FreeRTOS	 : SI (queues)

===============================================================================
#### adc_led ####
Practica realizada en clase. Utilizando el potenciometro del LPC 1769 BOARD prendemos y apagamos un led. Configurando un match del TIMER0, cada vez que el mismo interrumpe, dispara una conversion del ADC. Cuando el valor es leido, genera una interrupcion en la que leemos el valor del ADC y cargamos ese numero en una queue. Una tarea lo evaluara y prendera o apagara el led si supera o no cierto valor umbral.

* Board		 : LCP 1769 BOARD
* Peripherals	 : GPIO (led) - TIMER (IRQ) - ADC (IRQ) - potenciometro (LPC 1769 BOARD)
* FreeRTOS	 : SI (queue)

===============================================================================
### pwm ###
Proyecto realizado para probar el periferico PWM. Utilizando las interrupciones de PWM, se hace togglear un pin a partir de una funcion que indica durante cuanto tiempo estara habilitado y a que frecuencia. Hay problemas con el .h y .c correspondiente a pwm que esta incluido en la libreria chip, por lo tanto tuve que agregar los correspondientes archivos encontrados en internet.

* Board		 : --
* Peripherals	 : GPIO (led) - PWM (IRQ)
* FreeRTOS	 : SI (queues)

===============================================================================
#### eeprom_lpc1769 ####
Practica realizada en clase. Escribe la eeprom del stick LPC1769 mediante el periferico I2C y luego la lee para saber si se escribio correctamente.

* Board		 : --
* Peripherals 	 : GPIO (led) - I2S - EEPROM (del stick LPC1769)
* FreeRTOS	 : SI

===============================================================================
#### signalgenerator_dma_dac ####
Practica realizada en clase a partir del ejemplo dado por el ayudante de cátedra. Generador de señales precario. Cada cierto tiempo (4 segundos aprox) cambia la salida siguiendo esta secuencia: SENO -> TRIANGULAR -> RECTANGULAR. A partir de una tabla, utilizando el DMA, muestra la misma por el DAC del LPC. Tiene realizada una modificación en la función Chip_GPDMA_SGTransfer por eso se realizó una propia función.

* Board		 : --
* Peripherals	 : GPIO (led) - DMA - DAC
* FreeRTOS	 : NO

===============================================================================
#### adc_dac_loopback ####
Loopback entre el ADC y DAC del LPC 1769 utilizando interrupciones. Intente hacerlo por DMA, no funciono y lo deje ya no que no lo necesitaba para mi proyecto. Si alguien logra hacerlo andar, aviseme y lo agrego.

* Board		 : --
* Peripherals 	 : GPIO (led) - ADC (IRQ) - DAC
* FreeRTOS	 : NO

===============================================================================
### i2s_mixto ###
Proyecto realizado para probar la comunicacion con conversores externos (ADC y DAC) con el protocolo I2S de Philips. Utiliza interrupciones. Fue utilizado con un ADC PCM1802 y un DAC PCM1781, ambos de Texas Instruments. En cada interrupcion de I2S se toma una muestra y se la saca por el DAC interno del micro tambien.

fs = 32 KHz // bclk = fs * 64 = 2.048 MHz // mclk = fs * 384 = 12.288 MHz // Estas frecuencias se pueden cambiar, revisar manual del LPC1769.

* Board		 : --
* Peripherals 	 : GPIO (led) - I2S (tx y rx)
* FreeRTOS	 : NO

===============================================================================
### i2s_conversores_externos ###
Proyecto realizado para probar la comunicacion con conversores externos (ADC y DAC) con el protocolo I2S de Philips. Utiliza DMA. Fue utilizado con un ADC PCM1802 y un DAC PCM1781, ambos de Texas Instruments. En cada interrupcion de DMA se avisa por una queue a la tarea encargada de realizar el procesamiento la cual en este caso, solo realiza un loopback.

El bloque de muestras del DMA es de 8 muestras. 

fs = 32 KHz // bclk = fs * 64 = 2.048 MHz // mclk = fs * 384 = 12.288 MHz // Estas frecuencias se pueden cambiar, revisar manual del LPC1769.

* Board		 : --
* Peripherals 	 : GPIO (led) - I2S (tx y rx) - DMA
* FreeRTOS	 : SI (queues)

===============================================================================
#### test_dsp_pridolfi #####
Practica de Pablo Ridolfi que me fue facilitada por un compañero, no la utilice ni la probe para ver como funciona. Quizas a alguien le sirva.

* Board		 : --
* Peripherals	 : ADC - DAC
* FreeRTOS	 : NO

===============================================================================
### parcial1 ###
Parcial 1, cursada 2016 curso martes a la mañana. Aprobe con el mismo. No encuentro el enunciado, cuando lo resuelva para dar el final lo armare y volvere a resolver. Si alguien lo tiene, por favor, facilitemelo.

* Board		 : --
* Peripherals 	 : GPIO (led, button)
* FreeRTOS	 : SI (queues)

===============================================================================
### parcial2 ###
Parcial 2, cursada 2016 curso martes a la mañana. Enunciado en la carpeta "docs", archivo "parcial2_2016_tema80.pdf". No esta funcionando y tiene errores, pero aprobe con el mismo. Cuando prepare el final lo resolvere correctamente.

* Board		 : --
* Peripherals 	 : GPIO (led, button) - ADC (IRQ) - I2C (IRQ) - TIMER (IRQ)
* FreeRTOS	 : SI (queues - semaphores)

===============================================================================

