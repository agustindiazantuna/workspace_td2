/*
 * adc_dac_header.h
 *
 *  Created on: 10 de mar. de 2017
 *      Author: agustin
 */

#ifndef ADC_DAC_HEADER_H_
#define ADC_DAC_HEADER_H_


#include <header.h>


// ********* FUNCIONES ********** //

// ****************************** //

// ***************** DEFINES ***************** //

	// ADC
	#define ADC_FREQ 				32000										// PROBAR CAMBIANDO A 32000
	#define ADC_DMA_CANT_MUESTRAS 	8
	#define ADC_DMA_CHANNEL 		1

	// DAC
	#define DAC_FREQ 				ADC_FREQ
	#define DAC_DMA_CANT_MUESTRAS 	ADC_DMA_CANT_MUESTRAS
	#define DAC_DMA_CHANNEL 		0

	// Cambiar segun corresponda al conexionado con el conversor/periferico
	// DAC: PCM 1802
	#define I2SRX_CLK			0,23
	#define I2SRX_WS			0,24
	#define I2SRX_SDA			0,25
	#define RX_MCLK				4,28

	// DAC: PCM 1781
	#define I2STX_CLK			2,11
	#define I2STX_WS			2,12
	#define I2STX_SDA			2,13
	#define TX_MCLK				4,29

	#define CH_L				1
	#define CH_R				2

	#define BUFFER_A			0
	#define	BUFFER_B			1

	#define LED_stick			0,22


// ******************************************* //


// *************** ESTRUCTURAS *************** //


// ******************************************* //


// ************* VARIABLES GLOBALES ************* //

	// ADC
	extern volatile uint8_t canal_adc;
	extern DMA_TransferDescriptor_t DMA_descriptor_ADC_BUFFER;
	extern volatile uint32_t dma_memory_adc_BUFF[ADC_DMA_CANT_MUESTRAS];

	// DAC
	extern volatile uint8_t canal_dac;
	extern DMA_TransferDescriptor_t DMA_descriptor_DAC_BUFFER;
	extern volatile uint32_t dma_memory_dac_BUFF[ADC_DMA_CANT_MUESTRAS];

	// Queue
	extern volatile xQueueHandle Queue_new_data;

// ********************************************** //


// ************* HEADERS ************* //

		#include <adc_dac_init.h>

// ****************************************** //


#endif /* ADC_DAC_HEADER_H_ */
