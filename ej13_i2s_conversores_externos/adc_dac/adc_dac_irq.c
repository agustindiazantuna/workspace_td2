/*
 * adc_dac_irq.c
 *
 *  Created on: 10 de mar. de 2017
 *      Author: agustin
 */

#include "header.h"


void DMA_IRQHandler(void)
{
	portBASE_TYPE HPTW = pdFALSE;
	int buffer_ready = 1;

	if(Chip_GPDMA_Interrupt(LPC_GPDMA, canal_adc)) // Se fija si interrumpio el ADC
	{
		// Aviso a la tarea que puede procesar el buffer del ADC
		xQueueSendToBackFromISR(Queue_new_data, (const void * const) &buffer_ready, &HPTW);
		portEND_SWITCHING_ISR(HPTW);
	}

	if(Chip_GPDMA_Interrupt(LPC_GPDMA, canal_dac)) // Se fija si interrumpio el DAC
	{
		Chip_GPDMA_SGTransfer(LPC_GPDMA, canal_adc,
								&DMA_descriptor_ADC_BUFFER,
								GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA);
	}
}


