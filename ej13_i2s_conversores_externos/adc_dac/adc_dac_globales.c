/*
 * adc_dac_globales.c
 *
 *  Created on: 10 de mar. de 2017
 *      Author: agustin
 */


#include <header.h>


// ADC
volatile uint8_t canal_adc;

DMA_TransferDescriptor_t DMA_descriptor_ADC_BUFFER;

volatile uint32_t dma_memory_adc_BUFF[ADC_DMA_CANT_MUESTRAS];

// DAC
volatile uint8_t canal_dac;

DMA_TransferDescriptor_t DMA_descriptor_DAC_BUFFER;

volatile uint32_t dma_memory_dac_BUFF[ADC_DMA_CANT_MUESTRAS];



