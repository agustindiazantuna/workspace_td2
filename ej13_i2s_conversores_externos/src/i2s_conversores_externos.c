/*
====================================================================================================================
 Name        : i2s_conversores_externos.c
 Author      : Agustin Díaz Antuña
 Contacto	 : * audiodplab@frba.utn.edu.ar
 	 	 	   * Asunto: Workspace TD2 LPC1769
 Version     : 1.0
 Copyright   : $(copyright)
 Date		 : 02/08/16
 Description : Proyecto realizado para probar la comunicacion con conversores externos (ADC y DAC) con el protocolo
 	 	 	   I2S de Philips. Utiliza DMA.
 	 	 	   Fue utilizado con un ADC PCM1802 y un DAC PCM1781, ambos de Texas Instruments.
 	 	 	   En cada interrupcion de DMA se avisa por una queue a la tarea encargada de realizar el procesamiento
 	 	 	   la cual en este caso, solo realiza un loopback.
 	 	 	   El bloque de muestras del DMA es de 8 muestras.
 	 	 	   fs = 32 KHz
 	 	 	   bclk = fs * 64 = 2.048 MHz
 	 	 	   mclk = fs * 384 = 12.288 MHz
 	 	 	   Estas frecuencias se pueden cambiar, revisar manual del LPC1769.
 Board		 : --
 Peripherals : GPIO (led) - I2S (tx y rx) - DMA
 FreeRTOS	 : SI (queues)
====================================================================================================================
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <header.h>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Queue mediante la que aviso que llego un nuevo dato desde la interrupcion, podria usarse un semaforo
volatile xQueueHandle Queue_new_data;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Tareas
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// LED ALIVE
void vTaskLED_alive(void *pvParameters)
{
	while (1)
	{
		vTaskDelay( 1000 / portTICK_RATE_MS );
		Chip_GPIO_SetPinToggle(LPC_GPIO, LED_stick);
	}
}

// SIGNAL PROCESSING
void vTaskSIGNAL_PROC(void *pvParameters)
{
	uint32_t i = 0;
	uint8_t buffer_state = 0;

	while (1)
	{
		if(xQueueReceive(Queue_new_data, &buffer_state, portMAX_DELAY) == pdTRUE)
		{
			for(i = 0; i < DAC_DMA_CANT_MUESTRAS; i++)				// Copio los datos leidos en el buffer del dac
				dma_memory_dac_BUFF[i] = dma_memory_adc_BUFF[i];

			// Transmito el BUFFER
			Chip_GPDMA_SGTransfer(LPC_GPDMA, canal_dac,
									&DMA_descriptor_DAC_BUFFER,
									GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Inicializacion
void InitHardware(void)
{
	// Configuro el timer del micro en 120MHz						NO ANDA!
	Chip_SetupXtalClocking();
	Chip_SYSCTL_SetFLASHAccess(FLASHTIM_120MHZ_CPU);

	SystemCoreClockUpdate();

	Board_Init();

	// Creo queue
	Queue_new_data = xQueueCreate(4, sizeof(uint8_t));

	i2s_init();

	dma_init();

	// TAREA LED
	xTaskCreate(vTaskLED_alive, "vTaskLED_alive",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	// TAREA SIGNAL_PROCESSING
	xTaskCreate(vTaskSIGNAL_PROC, "vTaskSIGNAL_PROC",
				configMINIMAL_STACK_SIZE*8, NULL, (tskIDLE_PRIORITY + 3UL),
				(xTaskHandle *) NULL);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void)
{
	InitHardware();

	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}

