/*
 * config.h
 *
 *  Created on: 10 de mar. de 2017
 *      Author: agustin
 */

#ifndef CONFIG_H_
#define CONFIG_H_

	#include "FreeRTOS.h"
	#include "task.h"
	#include "board.h"
	#include "chip.h"
	#include "semphr.h"
	#include "queue.h"

	#ifndef ON
		#define ON	1
	#endif
	#ifndef OFF
		#define OFF	0
	#endif

// ***** PERIFERICOS (init) ***** //


// ****************************** //


// ***** UTILIDADES (main while) ***** //


// *********************************** //


// ******** Utilidades implicitas ******** //


// *************************************** //


// ***** FUNCIONES ****** //
	void main_init();
// ********************** //

#endif /* CONFIG_H_ */
