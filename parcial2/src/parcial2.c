/*
====================================================================================================================
 Name        : parcial2.c
 Author      : Agustin Díaz Antuña
 Contacto	 : * audiodplab@frba.utn.edu.ar
 	 	 	   * Asunto: Workspace TD2 LPC1769
 Version     : 1.0
 Copyright   : $(copyright)
 Date		 : 02/08/16
 Description : Parcial 2, cursada 2016 curso martes a la mañana.
 	 	 	   Enunciado en la carpeta "docs", archivo "parcial2_2016_tema80.pdf".
 	 	 	   No esta funcionando y tiene errores, pero aprobe con el mismo.
 	 	 	   Cuando prepare el final lo resolvere correctamente.
 Board		 : --
 Peripherals : GPIO (led, button) - ADC (IRQ) - I2C (IRQ) - TIMER (IRQ)
 FreeRTOS	 : SI (queues - semaphores)
====================================================================================================================
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "chip.h"
#include "board.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Led
#define ON			1
#define OFF			0

// Debounce
#define WAIT_1 0
#define VALIDATE_1 1
#define WAIT_0 2
#define VALIDATE_0 3

// GPIO's
#define LED				0,22					// NOTA: Los puertos estan elegidos al azar, salvo el del led
#define MOTOR			0,23
#define VENTILADOR		0,24

#define PUERTA			0,25
#define PULSADOR		0,26

#define boton_PUERTA	0
#define boton_PULSADOR	1

#define PUERTA_ABIERTA	0
#define PUERTA_CERRADA	1

#define ENCENDIDO		0
#define APAGADO			1

// I2S
#define SDA1			0,19
#define SCL1			0,20

#define SPEED_100KHZ				100000
#define I2C_SLAVE_EEPROM_ADDR		0x50

// Timer
#define TIMER_match0	0

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Enum
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	typedef enum {
		IDLE,
		MENOR_MENOS_1,
		MAYOR_5
	} estado_t;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Declaro las colas a utilizar
xQueueHandle sensor_q;
xQueueHandle puerta_q;
xQueueHandle pulsador_q;
xQueueHandle puerta_to_ctrl_q;
xQueueHandle puerta_to_i2c_q;

// Declaro los semaforos a utilizar
xSemaphoreHandle mediominuto_s;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Tasks
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ControlTask(void *pvParameters)
{
	estado_t ESTADO = IDLE;

	uint32_t temperatura = 0, puerta = PUERTA_ABIERTA, pulsador = APAGADO, cola = 0;

	while (1)
	{
		if( xQueueReceive(pulsador_q, &cola, portMAX_DELAY) )
		{
			if (pulsador == APAGADO)
				pulsador = ENCENDIDO;
			else
				pulsador = APAGADO;

			if (pulsador == APAGADO)									// Sistema apagado
			{
				Chip_GPIO_SetPinOutLow(LPC_GPIO, MOTOR);				// Apago el motor
				Chip_GPIO_SetPinOutLow(LPC_GPIO, VENTILADOR);			// Apago ventilador
			}
		}

		if (pulsador == ENCENDIDO)										// Sistema encedido
		{
			xQueueReceive(sensor_q, &temperatura, portMAX_DELAY);		// Recibo la temperatura de la IRQ

			switch(ESTADO)
			{
				case IDLE :
					if(temperatura > 10)								// Si la temperatura es mayor que 5
					{
						ESTADO = MAYOR_5;								// Cambio de estado
						Chip_GPIO_SetPinOutHigh(LPC_GPIO, MOTOR);		// Prendo el motor
						Chip_GPIO_SetPinOutLow(LPC_GPIO, VENTILADOR);	// Apago ventilador por si esta la puerta abierta
					}
					else if(temperatura < 4)							// Si la temperatura es menor que -1
					{
						ESTADO = MENOR_MENOS_1;							// Cambio de estado
						Chip_GPIO_SetPinOutLow(LPC_GPIO, MOTOR);		// Apago el motor por las dudas
						Chip_GPIO_SetPinOutLow(LPC_GPIO, VENTILADOR);	// Apago ventilador por las dudas
					}
				break;

				case MENOR_MENOS_1 :
					if(temperatura > 4)									// Si la temperatura ya es mayor que -1
					{
						ESTADO = IDLE;									// Cambio de estado
						Chip_GPIO_SetPinOutLow(LPC_GPIO, MOTOR);		// Apago el motor por las dudas
						Chip_GPIO_SetPinOutLow(LPC_GPIO, VENTILADOR);	// Apago ventilador por las dudas
					}
				break;

				case MAYOR_5 :
					if(temperatura < 10)
					{
						ESTADO = IDLE;
						Chip_GPIO_SetPinOutLow(LPC_GPIO, MOTOR);			// Apago el motor
						Chip_GPIO_SetPinOutLow(LPC_GPIO, VENTILADOR);		// Apago ventilador
					}
					if( xQueueReceive(puerta_q, &cola, portMAX_DELAY) )
					{
						if(puerta == PUERTA_ABIERTA)
						{
							Chip_GPIO_SetPinOutLow(LPC_GPIO, VENTILADOR);	// Apago ventilador
							puerta = PUERTA_CERRADA;
						}
						else
						{
							Chip_GPIO_SetPinOutHigh(LPC_GPIO, VENTILADOR);	// Prendo el ventilador
							puerta = PUERTA_ABIERTA;
						}
					}

				break;

				default:
					// control de errores, si llego lo hago
				break;
			}
		}
	}
}

void I2CTask(void *pvParameters)
{
	uint8_t buff[8], buffer_read_only[4];
	uint32_t cuenta = 0, puerta = 0, tiempo = 0;

	buff[0] = 0x0F;		// High
	buff[1] = 0xAF;		// Low
	buff[2] = 0x00;		// Dato tomado en 32 bits separado en 4 partes
	buff[4] = 0x00;
	buff[4] = 0x00;
	buff[6] = 0x00;

	Chip_I2C_MasterSend(I2C1, I2C_SLAVE_EEPROM_ADDR, buff, 2);				// No posicionamos en la posicion 0x0FAF
	Chip_I2C_MasterRead(I2C1, I2C_SLAVE_EEPROM_ADDR, buffer_read_only, 4);	// Leemos la cuenta de la EEPROM, 32 bits
	vTaskDelay( 1000 / portTICK_RATE_MS );									// Demora por lo lenta que es la memoria

	cuenta = cuenta + (buffer_read_only[0]<<24) + (buffer_read_only[1]<<16) + (buffer_read_only[2]<<8) + (buffer_read_only[0]<<0);

	while (1) {

		if( xQueueReceive(puerta_to_i2c_q, &puerta, portMAX_DELAY) )
		{
			cuenta = cuenta + puerta;
		}

		if ( xSemaphoreTake(mediominuto_s, portMAX_DELAY) )
		{
			tiempo ++;

			if(tiempo == 10)							// 5 minutos
			{
				tiempo = 0;

				buff[2] = (cuenta >> 24);
				buff[3] = (cuenta >> 16);
				buff[4] = (cuenta >> 8);
				buff[5] = (cuenta >> 0);

				Chip_I2C_MasterSend(I2C1, I2C_SLAVE_EEPROM_ADDR, buff, 6);
				vTaskDelay( 1000 / portTICK_RATE_MS );
			}
		}


	}
}

void DEBOUNCETask(void *pvParameters)
{
	int EstadoDebounce = WAIT_1, tecla_actual = 0, tecla_anterior = 0;

	while (1)
	{
		switch(EstadoDebounce)
		{
		case WAIT_1:
			if( Chip_GPIO_GetPinState( LPC_GPIO , PUERTA ) == 1 )		// Puerta normal cerrado, pulsador puesto a GND cuando esta cerrado
					tecla_actual = boton_PUERTA;
			if( Chip_GPIO_GetPinState( LPC_GPIO , PULSADOR ) == 1 )
					tecla_actual = boton_PULSADOR;

			if( tecla_actual != 0)
				EstadoDebounce = VALIDATE_1;

			tecla_anterior = tecla_actual;
			break;
		case VALIDATE_1:
			vTaskDelay(50/portTICK_RATE_MS);

			if( Chip_GPIO_GetPinState( LPC_GPIO , PUERTA ) == 0 )
					tecla_actual = boton_PUERTA;
			if( Chip_GPIO_GetPinState( LPC_GPIO , PULSADOR ) == 0 )
					tecla_actual = boton_PULSADOR;

			if( tecla_anterior == tecla_actual)
			{
				EstadoDebounce = WAIT_0;

				// Abrieron la puerta de la heladera, aviso
				if (tecla_anterior == boton_PUERTA)
				{
					xQueueSendToBack( puerta_q, &tecla_actual, portMAX_DELAY );
					xQueueSendToBack( puerta_to_i2c_q, &tecla_actual, portMAX_DELAY );
				}
				// Pulsaron el boton de encendido
				else if (tecla_anterior == boton_PULSADOR)
				{
					xQueueSendToBack( pulsador_q, &tecla_actual, portMAX_DELAY );
				}

			}
			else
				EstadoDebounce = WAIT_1;

			tecla_actual = 0;
			break;
		case WAIT_0:
			if( Chip_GPIO_GetPinState( LPC_GPIO , PUERTA ) == 0 )
					tecla_actual = boton_PUERTA;
			if( Chip_GPIO_GetPinState( LPC_GPIO , PULSADOR ) == 0 )
					tecla_actual = boton_PULSADOR;

			if( tecla_actual == 0 )
				EstadoDebounce = VALIDATE_0;

			tecla_actual = 0;
			break;
		case VALIDATE_0:
			vTaskDelay(50/portTICK_RATE_MS);

			if( Chip_GPIO_GetPinState( LPC_GPIO , PUERTA ) == 0 )
					tecla_actual = boton_PUERTA;
			if( Chip_GPIO_GetPinState( LPC_GPIO , PULSADOR ) == 0 )
					tecla_actual = boton_PULSADOR;

			if( 0 == tecla_actual)
				EstadoDebounce = WAIT_1;
			else
				EstadoDebounce = WAIT_0;

			tecla_actual = 0;
			break;
		default:
			break;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Functions
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void prvSetupHardware(void)
{
	SystemCoreClockUpdate();

	// Inicialización de la placa LPC Board
    Board_Init();


	// LED
	Chip_IOCON_PinMux(LPC_IOCON, LED, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED);

	// MOTOR
	Chip_IOCON_PinMux(LPC_IOCON, MOTOR, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, MOTOR);

	// VENTILADOR
	Chip_IOCON_PinMux(LPC_IOCON, VENTILADOR, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, VENTILADOR);

	// PUERTA
	Chip_IOCON_PinMux(LPC_IOCON, PUERTA, MD_PUP, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, PUERTA);

	// PULSADOR
	Chip_IOCON_PinMux(LPC_IOCON, PULSADOR, MD_PUP, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, PULSADOR);


	// Estado inicial
	Chip_GPIO_SetPinState(LPC_GPIO, LED, OFF);
	Chip_GPIO_SetPinState(LPC_GPIO, MOTOR, OFF);
	Chip_GPIO_SetPinState(LPC_GPIO, VENTILADOR, OFF);


	// TIMER0
	Chip_TIMER_Init(LPC_TIMER0);

	Chip_Clock_SetPCLKDiv(SYSCTL_PCLK_TIMER0, SYSCTL_CLKDIV_1);					// Seteo frecuencia 96 MHz, igual que la del sistema
	Chip_TIMER_PrescaleSet(LPC_TIMER0, 0);

	Chip_TIMER_SetMatch(LPC_TIMER0, TIMER_match0, 2880000000);					// Match 0: 30 segundos
	Chip_TIMER_MatchEnableInt(LPC_TIMER0, TIMER_match0);						// Habilito interrupción
	Chip_TIMER_ResetOnMatchEnable(LPC_TIMER0, TIMER_match0);					// No resetea en MO
	Chip_TIMER_StopOnMatchDisable(LPC_TIMER0, TIMER_match0);					// No detiene la cuenta en MO

	Chip_TIMER_Enable(LPC_TIMER0);												// Disparo timer

	NVIC_EnableIRQ(TIMER0_IRQn);												// Habilito la interrupcion


	// I2C
	Chip_IOCON_PinMux(LPC_IOCON, SDA1, IOCON_MODE_INACT, IOCON_FUNC3);
	Chip_IOCON_PinMux(LPC_IOCON, SCL1, IOCON_MODE_INACT, IOCON_FUNC3);
	Chip_IOCON_EnableOD(LPC_IOCON, SDA1);
	Chip_IOCON_EnableOD(LPC_IOCON, SCL1);

	Chip_I2C_Init(I2C1);
	Chip_I2C_SetClockRate(I2C1, SPEED_100KHZ);
	Chip_I2C_SetMasterEventHandler(I2C1, Chip_I2C_EventHandler);

	NVIC_EnableIRQ(I2C1_IRQn);


	// ADC
	ADC_CLOCK_SETUP_T adc;

	Chip_ADC_Init(LPC_ADC, &adc);
	Chip_ADC_SetSampleRate(LPC_ADC, &adc, 10);					// Cada 100 ms chequeo la temperatura -> 10 Hz
	Chip_ADC_EnableChannel(LPC_ADC, ADC_CH0, ENABLE);
	Chip_ADC_Int_SetChannelCmd(LPC_ADC, ADC_CH0, ENABLE);
	Chip_ADC_SetBurstCmd(LPC_ADC, ENABLE);

	NVIC_EnableIRQ(ADC_IRQn);

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// IRWHandler
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ADC_IRQHandler(void)
{
	uint16_t data = 0;
	portBASE_TYPE HPTW = pdFALSE;

	Chip_ADC_ReadValue(LPC_ADC, ADC_CH0, &data);

	xQueueSendToBackFromISR(sensor_q, &data, &HPTW);
	portEND_SWITCHING_ISR(HPTW);

}

void I2C1_IRQHandler(void)
{
	Chip_I2C_MasterStateHandler(I2C1);
}

void TIMER0_IRQHandler(void)
{
	portBASE_TYPE HPTW = pdFALSE;

	// Nos fijamos si fue interrupción por el match
	if(Chip_TIMER_MatchPending(LPC_TIMER0, TIMER_match0))
	{
		xSemaphoreGiveFromISR(mediominuto_s, &HPTW);
		portEND_SWITCHING_ISR(HPTW);

		Chip_TIMER_ClearMatch(LPC_TIMER0, TIMER_match0);			// Borramos el flag del M0
	}

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void)
{
	// Variables
	sensor_q = xQueueCreate(1,sizeof(uint32_t));			// Creo cola para que la IRQ del ADC le comunique la T a la tarea ControlTask
	puerta_to_ctrl_q = xQueueCreate(1,sizeof(uint32_t));	// Creo cola para que la tarea de botones le comunique el estado de la puerta a la tarea ControlTask
	puerta_to_i2c_q = xQueueCreate(1,sizeof(uint32_t));		// Creo cola para que la tarea de botones le comunique el estado de la puerta a la tarea I2C
	pulsador_q = xQueueCreate(1,sizeof(uint32_t));			// Creo cola para que la tarea de botones le comunique el estado del interruptor a la tarea ControlTask

	vSemaphoreCreateBinary(mediominuto_s);

	// Inicialización
	prvSetupHardware();

	// Tareas
	xTaskCreate(DEBOUNCETask, "DEBOUNCETask",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate(I2CTask, "I2CTask",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate(ControlTask, "ControlTask",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}

