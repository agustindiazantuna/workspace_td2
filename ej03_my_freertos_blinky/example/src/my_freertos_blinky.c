/*
====================================================================================================================
 Name        : my_freertos_blinky.c
 Author      : Agustin Díaz Antuña
 Contacto	 : * audiodplab@frba.utn.edu.ar
 	 	 	   * Asunto: Workspace TD2 LPC1769
 Version     : 1.0
 Copyright   : $(copyright)
 Date		 : --
 Description : Practica realizada en clase.
 	 	 	   Punto A:
 	 	 	   	   	   Parpadea cada 1 segundo dentro de una tarea usando vTaskDelay.
 	 	 	   	   	   Colocar el "#define PUNTO_A" en 1 y el resto en 0.
 	 	 	   Punto A1:
 	 	 	   	   	   Parpadea a otra frecuencia utilizando funciones para setear un 1 o un 0 en vez de utilizar la
 	 	 	   	   	   función toggle.
 	 	 	   	   	   Colocar el "#define PUNTO_A1" en 1 y el resto en 0.
 	 	 	   Punto B:
 	 	 	   	   	   Queremos que parpadee a una frecuencia de 1uS. Pero no se puede hacer con el vTaskDelay ya que
 	 	 	   	   	   su base de tiempo es mayor. Entonces vamos a usar un timer y en su interrupción hacemos que el
 	 	 	   	   	   led togglee.
 	 	 	   	   	   Tiene otro tiempo de match comentado para que sea visual, cambiar si así lo desea.
 	 	 	   	   	   Colocar el "#define PUNTO_B" en 1 y el resto en 0.
 	 	 	   Punto B1:
 	 	 	   	   	   Idem al punto B.
 	 	 	   	   	   Solo que ahora queremos que parpadee durante 5 segundos y durante otros 5 segundos no lo haga.
 	 	 	   	   	   Usamos vTaskDelay para ello.
 	 	 	   	   	   Colocar el "#define PUNTO_B1" en 1 y el resto en 0.
 	 	 	   Punto C:
 	 	 	   	   	   Hace lo mismo que el punto B, pero ahora el toggleo lo hacemos en la tarea.
 	 	 	   	   	   En la interrupción, a traves de un semaforo, le avisamos a la tarea que el tiempo del match
 	 	 	   	   	   ha transcurrido.
 	 	 	   	   	   Colocar el "#define PUNTO_C" en 1 y el resto en 0.
 Board		 : --
 Peripherals : GPIO (led) - TIMER (IRQ)
 FreeRTOS	 : SI (semaphore )
====================================================================================================================
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define LED_port		0
#define LED_pin			22
#define LED_on			1
#define LED_off			0
#define TIMER_match0	0

#define PUNTO_A			1
#define PUNTO_A1		0
#define PUNTO_B			0
#define PUNTO_B1		0
#define PUNTO_C			0

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

xSemaphoreHandle SEM_led;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Inicializacion
void prvSetupHardware(void)
{
	SystemCoreClockUpdate();

	// Configuramos los pines para todos los puntos.
	// Led del stick P0,22
	Chip_IOCON_PinMux(LPC_IOCON, LED_port, LED_pin, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_port, LED_pin);

	#if (PUNTO_B == 1 || PUNTO_B1 == 1 || PUNTO_C == 1)
		// Configuramos un timer.
		// Inicializamos, energizamos el periférico.
		Chip_TIMER_Init(LPC_TIMER0);

		// Le damos una frecuencia.
		Chip_Clock_SetPCLKDiv(SYSCTL_PCLK_TIMER0, SYSCTL_CLKDIV_1);
		Chip_TIMER_PrescaleSet(LPC_TIMER0, 0);

		// Configuramos el match.
		#if (PUNTO_B == 1 || PUNTO_B1 == 1)
//			Chip_TIMER_SetMatch(LPC_TIMER0, TIMER_match0, SystemCoreClock / 1000000);	// 1uS * Main Clock
			Chip_TIMER_SetMatch(LPC_TIMER0, TIMER_match0, SystemCoreClock / 10);		// 100mS * Main Clock para verlo visualmente
		#elif (PUNTO_C == 1)
			Chip_TIMER_SetMatch(LPC_TIMER0, TIMER_match0, SystemCoreClock / 1);			// 1S * Main Clock
		#endif

		Chip_TIMER_MatchEnableInt(LPC_TIMER0, TIMER_match0);
		Chip_TIMER_ResetOnMatchEnable(LPC_TIMER0, TIMER_match0);
		Chip_TIMER_StopOnMatchDisable(LPC_TIMER0, TIMER_match0);
		Chip_TIMER_ExtMatchControlSet(LPC_TIMER0, 0, TIMER_EXTMATCH_DO_NOTHING, 0);

		// Disparamos el timer.
		Chip_TIMER_Enable(LPC_TIMER0);

		// Habilitamos las interrupciones.
		NVIC_EnableIRQ(TIMER0_IRQn);
	#endif
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Interrupciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// IRQ Timer 0
#if (PUNTO_B == 1 || PUNTO_B1 == 1 || PUNTO_C == 1)

	void TIMER0_IRQHandler(void)
	{
		portBASE_TYPE pxHigherPriorityTaskWoken = pdFALSE;

		// Nos fijamos si fue interrupción por el match
		if(Chip_TIMER_MatchPending(LPC_TIMER0, TIMER_match0))
		{
			#if (PUNTO_B == 1 || PUNTO_B1 == 1)
					// Hacemos togglear el led
					Chip_GPIO_SetPinToggle(LPC_GPIO, LED_port, LED_pin);
			#elif (PUNTO_C == 1)
					/* Esto es para el caso de inversión de prioridades. Ejemplo: Una tarea de prioridad alta está bloqueada
					 * y una de prioridad baja está ejecutándose con un semáforo tomado. Llega la interrupción y tiene que hacer
					 * un cambio de contexto para la tarea de mayor prioridad. En la variable extra nos devuelve ese valor de prioridad.
					 * La inicializamos en false, si el OS la cambia a true, es que hubo un cambio de contexto.
					 * */
					xSemaphoreGiveFromISR(SEM_led, &pxHigherPriorityTaskWoken);
					// Función encargada de verificar si se levantó una tarea de mayor prioridad y de ejecutar un cambio de contexto.
					portEND_SWITCHING_ISR(pxHigherPriorityTaskWoken);

			#endif

			// Borramos el match
			Chip_TIMER_ClearMatch(LPC_TIMER0, TIMER_match0);
		}
	}

#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Tareas
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Tarea LED
static void vLEDTask1(void *pvParameters)
{
	// Estado inicial
	Chip_GPIO_SetPinState(LPC_GPIO, LED_port, LED_pin, 0);

	while (1)
	{
		#if (PUNTO_A == 1)

			/*
			 * El scheduler la saca del estado running, la vuelve a ese estado cuando vence el tiempo del delay.
			 * vTaskDelay y todo el OS trabaja como unidad los TICKS, entonces para hacerlo amigable, usamos esos
			 * defines para pasarlo a segundos.
			 * El número por el que dividimos es el período.
			 * */

			vTaskDelay( 1000 / portTICK_RATE_MS );
			Chip_GPIO_SetPinToggle(LPC_GPIO, LED_port, LED_pin);

		#elif (PUNTO_A1 == 1)
			// OTRA FORMA DE HACER EL A)
		/* About a 3Hz on/off toggle rate */
			Chip_GPIO_SetPinState(LPC_GPIO, LED_port, LED_pin, 0);
			vTaskDelay(configTICK_RATE_HZ / 6);
			Chip_GPIO_SetPinState(LPC_GPIO, LED_port, LED_pin, 1);
			vTaskDelay(configTICK_RATE_HZ / 6);

		#endif

		/* PUNTO (B) 1 micro segundo, pero NO FUNCIONA porque esos tiempos son muy pequeños en comparación con el tick.
		vTaskDelay( portTICK_RATE_MS / 100 );
		Chip_GPIO_SetPinToggle(LPC_GPIO, LED_port, LED_pin);
		*/

		#if(PUNTO_B1 == 1)
		//PUNTO (B.1) para que parpadee cada 1uS durante 5S y durante otros 5S deje de hacerlo
			NVIC_EnableIRQ(TIMER0_IRQn);
			vTaskDelay(5000 / portTICK_RATE_MS);
			NVIC_DisableIRQ(TIMER0_IRQn);
			vTaskDelay(5000 / portTICK_RATE_MS);
		#endif

		#if (PUNTO_C == 1)
			/* PUNTO (C)
			 * La tarea toma el semáforo, ejecuta el código a continuación, sigue en el while(1) y cuando quiere
			 * volver a tomarlo, como ya está tomado, se bloquea, esperando a que el mismo sea liberado por la
			 * interrupción.
			*/

			// Tomamos el semáforo, y ponemos el delay máximo, el tiempo que le damos al semáforo, si no lo liberan
			// en ese tiempo, entonces la taera continua con la ejecución de la siguiente línea.
			xSemaphoreTake(SEM_led, portMAX_DELAY);

			Chip_GPIO_SetPinToggle(LPC_GPIO, LED_port, LED_pin);
		#endif
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void)
{
	#if (PUNTO_C == 1)
		// Creamos el semáforo.
		vSemaphoreCreateBinary(SEM_led);
	#endif

	prvSetupHardware();

	/* LED1 toggle thread */
	/* Parámetros: Nombre de la tarea, es el puntero a función donde está la tarea
				   Nombre de la tarea para debuggear.
				   Espacio asignado al stack correspondiente para la tarea, para sus registros y variables.
				   Parámetros que recibe la tarea al momento de su creación.
				   Prioridad de la tarea.
				   Manejador de la tarea en caso de que queramos hacerle alguna modificación.
	*/
	xTaskCreate(vLEDTask1, (signed char *) "vTaskLed1",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}


