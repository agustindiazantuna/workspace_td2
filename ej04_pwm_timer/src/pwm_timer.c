/*
====================================================================================================================
 Name        : pwm_timer.c
 Author      : Agustin Díaz Antuña
 Contacto	 : * audiodplab@frba.utn.edu.ar
 	 	 	   * Asunto: Workspace TD2 LPC1769
 Version     : 1.0
 Copyright   : $(copyright)
 Date		 : 23/08/16
 Description : Practica realizada en clase.
 	 	 	   Punto 1:
 	 	 	   	   	   Usando el timer 0 y dos niveles de match, se hace parpadear
 	 	 	   	   	   al led en base a la frecuencia elegida, toggleando el estado
 	 	 	   	   	   del led en cada IRQ.
 	 	 	   	   	   No usa FreeRTOS.
 	 	 	   	   	   Colocar el "#define PUNTO_1" en 1 y el resto en 0.
 	 	 	   Punto 2:
 	 	 	   	   	   Usando el timer 0 y dos niveles de match, cuando interrupe
 	 	 	   	   	   por cada match se entrega un semaforo para que una tarea prenda
 	 	 	   	   	   y otra apague el led segun que match provoco la IRQ.
 	 	 	   	   	   Colocar el "#define PUNTO_2" en 1 y el resto en 0.
 	 	 	   Punto 3:
 	 	 	   	   	   Hace lo mismo que el punto 2, agregando una tarea que hace
 	 	 	   	   	   parpadear el led del stick a otra frecuencia.
 	 	 	   	   	   Colocar el "#define PUNTO_3" en 1 y el resto en 0.
 Board		 : --
 Peripherals : GPIO (led) - TIMER (IRQ)
 FreeRTOS	 : SI (semaphore)
====================================================================================================================
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define PUNTO_1				0
#define PUNTO_2				0
#define PUNTO_3				1

#define TIMER_match0		0
#define TIMER_match1		1

#define MATCH0				20000
#define MATCH1				10000

#if PUNTO_1
	#define PWM				0,22
#elif PUNTO_2
	#define PWM				0,22
#elif PUNTO_3
	#define LED_stick		0,22
	#define LED_on			1
	#define LED_off			0
	#define PWM				2,13
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

xSemaphoreHandle xSEM_pinlow;
xSemaphoreHandle xSEM_pinhigh;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Inicializacion
void prvSetupHardware(void)
{
	SystemCoreClockUpdate();

	// Configuramos los pines.
	#if PUNTO_3
		Chip_IOCON_PinMux(LPC_IOCON, LED_stick, MD_PLN, IOCON_FUNC0);
		Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_stick);
	#endif

	Chip_IOCON_PinMux(LPC_IOCON, PWM, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, PWM);

	// Configuramos el timer 0
	// Inicializamos, energizamos el periférico.
	Chip_TIMER_Init(LPC_TIMER0);

	// Le damos una frecuencia.
	Chip_Clock_SetPCLKDiv(SYSCTL_PCLK_TIMER0, SYSCTL_CLKDIV_1);
	Chip_TIMER_PrescaleSet(LPC_TIMER0, 0);

	// Configuramos el match0
	Chip_TIMER_SetMatch(LPC_TIMER0, TIMER_match0, SystemCoreClock / MATCH0);	// Tiempo: 10KHz
	Chip_TIMER_MatchEnableInt(LPC_TIMER0, TIMER_match0);						// Habilito interrupción
	Chip_TIMER_ResetOnMatchDisable(LPC_TIMER0, TIMER_match0);					// No resetea en MO
	Chip_TIMER_StopOnMatchDisable(LPC_TIMER0, TIMER_match0);					// No detiene la cuenta en MO

	// Configuramos el match1
	Chip_TIMER_SetMatch(LPC_TIMER0, TIMER_match1, SystemCoreClock / MATCH1);	// Tiempo: 2*10KHz
	Chip_TIMER_MatchEnableInt(LPC_TIMER0, TIMER_match1);						// Habilito interrupción
	Chip_TIMER_ResetOnMatchEnable(LPC_TIMER0, TIMER_match1);					// Resetea en M1
	Chip_TIMER_StopOnMatchDisable(LPC_TIMER0, TIMER_match1);					// No detiene la cuenta en M1

	// Disparamos el timer.
	Chip_TIMER_Enable(LPC_TIMER0);

	// Habilitamos las interrupciones.
	NVIC_EnableIRQ(TIMER0_IRQn);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Interrupciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Interrupción del timer0
void TIMER0_IRQHandler(void)
{
	#if PUNTO_2 || PUNTO_3
		portBASE_TYPE HPTW = pdFALSE;
	#endif

	// Nos fijamos si fue interrupción por el match
	if(Chip_TIMER_MatchPending(LPC_TIMER0, TIMER_match0))
	{
		#if PUNTO_1
				Chip_GPIO_SetPinOutLow(LPC_GPIO, PWM);
		#endif

		#if PUNTO_2 || PUNTO_3
				xSemaphoreGiveFromISR(xSEM_pinlow, &HPTW);
				portEND_SWITCHING_ISR(HPTW);
		#endif

		Chip_TIMER_ClearMatch(LPC_TIMER0, TIMER_match0);			// Borramos el flag del M0
	}
	else if(Chip_TIMER_MatchPending(LPC_TIMER0, TIMER_match1))
	{
		#if	PUNTO_1
				Chip_GPIO_SetPinOutHigh(LPC_GPIO, PWM);
		#endif

		#if PUNTO_2 || PUNTO_3
				xSemaphoreGiveFromISR(xSEM_pinhigh, &HPTW);
				portEND_SWITCHING_ISR(HPTW);
		#endif

		Chip_TIMER_ClearMatch(LPC_TIMER0, TIMER_match1);			// Borramos el flag del M1
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Tareas
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if PUNTO_2 || PUNTO_3
	// Tarea para apagar el led
	void vTaskPINLOW(void *pvParameters)
	{
		while (1)
		{
			xSemaphoreTake(xSEM_pinlow, portMAX_DELAY);
			Chip_GPIO_SetPinOutLow(LPC_GPIO, PWM);
		}
	}

	// Tarea para prender el led
	void vTaskPINHIGH(void *pvParameters)
	{
		while (1)
		{
			xSemaphoreTake(xSEM_pinhigh, portMAX_DELAY);
			Chip_GPIO_SetPinOutHigh(LPC_GPIO, PWM);
		}
	}
#endif

#if PUNTO_3
	// LED toggle thread
	void vTaskLED(void *pvParameters)
	{
		// Estado inicial
		Chip_GPIO_SetPinState(LPC_GPIO, LED_stick, LED_off);

		while (1)
		{
			vTaskDelay( 1000 / portTICK_RATE_MS );
			Chip_GPIO_SetPinToggle(LPC_GPIO, LED_stick);
		}
	}
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void)
{
	// Variables
	#if PUNTO_2 || PUNTO_3
		vSemaphoreCreateBinary(xSEM_pinlow);
		vSemaphoreCreateBinary(xSEM_pinhigh);
	#endif

	// Inicializacion
	prvSetupHardware();

	// Tareas
	#if PUNTO_2 || PUNTO_3
		// Tarea para apagar el led
		xTaskCreate(vTaskPINLOW, "vTaskPINLOW",
					configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
					(xTaskHandle *) NULL);

		// Tarea para prender el led
		xTaskCreate(vTaskPINHIGH, "vTaskPINHIGH",
					configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
					(xTaskHandle *) NULL);
	#endif

	#if PUNTO_3
		// TAREA LED
		xTaskCreate(vTaskLED, "vTaskLED",
					configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
					(xTaskHandle *) NULL);
	#endif

	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}

