/*
====================================================================================================================
 Name        : adc_dac_loopback.c
 Author      : Agustin Díaz Antuña
 Contacto	 : * audiodplab@frba.utn.edu.ar
 	 	 	   * Asunto: Workspace TD2 LPC1769
 Version     : 1.0
 Copyright   : $(copyright)
 Date		 :
 Description : Loopback entre el ADC y DAC del LPC 1769 utilizando interrupciones.
 	 	 	   Intente hacerlo por DMA, no funciono y lo deje ya no que no lo necesitaba para mi proyecto.
 	 	 	   Si alguien logra hacerlo andar, aviseme y lo agrego.
 Board		 : --
 Peripherals : GPIO (led) - ADC (IRQ) - DAC
 FreeRTOS	 : NO
====================================================================================================================
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "board.h"
#include "chip.h"
//#include <cr_section_macros.h>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define LOOPBACK_SIN_DMA	1
#define	LOOPBACK_CON_DMA	~LOOPBACK_SIN_DMA

#define LED_port			0
#define LED_pin				22
#define LED_on				1
#define LED_off				0

#define LEN					1

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const uint32_t OscRateIn = 12000000;
const uint32_t RTCOscRateIn = 32768;

volatile uint8_t channelTC, channelTC1, dmaChannelNum_adc, dmaChannelNum_dac;
uint16_t DMAbuffer[LEN], DMAbuffer1;
uint16_t data;
uint16_t adcFlag = 0;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Inicializacion
void InitHardware(void)
{
	SystemCoreClockUpdate();

	// Led del stick P0,22
	Chip_IOCON_PinMux(LPC_IOCON, LED_port, LED_pin, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_port, LED_pin);

	// Init ADC
	ADC_CLOCK_SETUP_T adc;

	Chip_ADC_Init(LPC_ADC, &adc);
	Chip_ADC_SetSampleRate(LPC_ADC, &adc, 20000);
	Chip_ADC_EnableChannel(LPC_ADC, ADC_CH0, ENABLE);
	Chip_ADC_Int_SetChannelCmd(LPC_ADC, ADC_CH0, ENABLE);
	Chip_ADC_SetBurstCmd(LPC_ADC, ENABLE);

	#if LOOPBACK_SIN_DMA == 1
		NVIC_EnableIRQ(ADC_IRQn);
	#elif LOOPBACK_CON_DMA
		NVIC_DisableIRQ(ADC_IRQn);			// Deshabilito para usar el DMA
	#endif

	// Init DAC
	Chip_DAC_Init(LPC_DAC);

	#if LOOPBACK_CON_DMA == 1
		// Init DMA
		Chip_GPDMA_Init(LPC_GPDMA);

		// Pido un canal libre del DMA
		dmaChannelNum_adc = Chip_GPDMA_GetFreeChannel(LPC_GPDMA, 0);

		// Habilito la interrupcion del DMA
		NVIC_EnableIRQ(DMA_IRQn);

		Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNum_adc, GPDMA_CONN_ADC, (uint32_t) DMAbuffer,
							GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA, LEN);
	#endif

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Interrupciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if LOOPBACK_CON_DMA == 1
	void DMA_IRQHandler(void)
	{
		if (Chip_GPDMA_IntGetStatus(LPC_GPDMA,GPDMA_STAT_INT, dmaChannelNum_adc))				//Verifico que la interrupcion sea del canal 0 del DMA
		{
			if(Chip_GPDMA_IntGetStatus(LPC_GPDMA, GPDMA_STAT_INTTC, dmaChannelNum_adc))			//Verifico fin de DMA
			{
				Chip_GPDMA_ClearIntPending(LPC_GPDMA, GPDMA_STATCLR_INTTC, dmaChannelNum_adc);	//Limpio el flag de interrupción
				adcFlag = 1;

				Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNum_adc, GPDMA_CONN_ADC, (uint32_t) DMAbuffer,
									GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA, LEN);
			}
		}
	}
#endif

void ADC_IRQHandler(void)
{
	Chip_ADC_ReadValue(LPC_ADC, ADC_CH0, &data);

	adcFlag=1;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void)
{
	uint32_t j = 0;

	#if (LOOPBACK_CON_DMA == 1)
		uint32_t dataADC[LEN];
		uint32_t i = 0;
	#endif

	InitHardware();

	while(1)
	{
		#if	(LOOPBACK_SIN_DMA == 1)
			if(adcFlag)
			{
				Chip_DAC_UpdateValue(LPC_DAC, data);
				adcFlag = 0;
			}
		#elif (LOOPBACK_CON_DMA == 1)
			if(adcFlag)
			{
				channelTC = 0;

				for(i=0;i<LEN;i++)
				{
					// El ADC guarda la informacion convertida en la parte menos significativa del dato.
					// La funcion que escribe el DAC debe recibir la informacion en la parte mas significativa del dato.
					// Por eso se lo desplaza.
					// Recordar que el DAC es de 10 bits
					dataADC[i] = ((DMAbuffer[i]) << 6);

					// En caso de realizarse algun procesamiento de la señal deberia realizarse aqui.
					// Por ejemplo un filtro de media movil para eliminar los glitches del ADC.

					// Escribo en memoria para que vaya al dac
					Chip_DAC_UpdateValue(LPC_DAC, dataADC[i]);
				}
					adcFlag = 0;
			}
		#endif
		if(j == 500000)		// Demora rustica para ver parpadear el led indicando que todo anda  bien
		{
			Chip_GPIO_SetPinToggle(LPC_GPIO, LED_port, LED_pin);
			j = 0;
		}
		j++;
	}

	// Should never arrive here
	return 1;
}

