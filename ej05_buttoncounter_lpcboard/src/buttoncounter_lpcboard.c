/*
====================================================================================================================
 Name        : buttoncounter_lpcboard.c
 Author      : Agustin Díaz Antuña
 Contacto	 : * audiodplab@frba.utn.edu.ar
 	 	 	   * Asunto: Workspace TD2 LPC1769
 Version     : 1.0
 Copyright   : $(copyright)
 Date		 : 02/08/16
 Description : Practica realizada en clase.
 	 	 	   Al presionar el boton CTRL comenzamos la cuenta, con el botón IN
 	 	 	   aumentamos el numero contado. Al volver a apretar el boton CTRL
 	 	 	   un led parpadea el valor almacenado.
 Board		 : LPC 1769 BOARD
 Peripherals : GPIO (botones, led)
 FreeRTOS	 : SI (queues)
====================================================================================================================
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "chip.h"
#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define LEDStick_port	0
#define LEDStick_pin	22
#define LED_on			1
#define LED_off			0
#define BCTRL_port		1
#define BCTRL_pin		31
#define BIN_port		2
#define BIN_pin			10

#define NO_CUENTA			0
#define CUENTA				1
#define TERMINO_CUENTA		2

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

xQueueHandle queue_led;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Tareas
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Tarea para la lectura de los botones
static void vBTask(void *pvParameters)
{
	int estado = 0, input = 0, blink = 0, botonctrl = 0;

	while (1)
	{
		botonctrl = Chip_GPIO_GetPinState(LPC_GPIO, BCTRL_port, BCTRL_pin);				// Leo boton
		if(botonctrl == 0 && estado == NO_CUENTA)										// Veo cuantas veces se presiono el boton
			estado = CUENTA;
		else if (botonctrl == 0 && estado == CUENTA)
			estado = TERMINO_CUENTA;

		if(estado == CUENTA)															// Incremento la cuenta
		{
			input = Chip_GPIO_GetPinState(LPC_GPIO, BIN_port, BIN_pin);
			vTaskDelay( 100 / portTICK_RATE_MS );
			if(input == 0)
				blink ++;
		}

		if(estado == TERMINO_CUENTA)
		{
			xQueueSendToBack(queue_led, &blink, portMAX_DELAY);					// Cargo en la queue el valor contado
			estado = NO_CUENTA;
			blink = 0;
		}
	}
}

// Tarea para parpadear el led
static void vLEDTask(void *pvParameters)
{
	int dato = 0;

	while (1)
	{
		if (xQueueReceive(queue_led, &dato, portMAX_DELAY) == pdTRUE)			// Si la queue tiene algo para leer, xQueueReceive devuelve un pdTRUE entonces cumple el if y saca el dato
		{
			dato = dato*2;														// Multiplico para que parpadee
			vTaskDelay( 1000 / portTICK_RATE_MS );
			Chip_GPIO_SetPinToggle(LPC_GPIO, LEDStick_port, LEDStick_pin);
			dato --;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Inicializacion del hardware
static void prvSetupHardware(void)
{
	SystemCoreClockUpdate();

	// Inicialización de la placa
    Board_Init();

    // Set the LED to the state of "On"
    Board_LED_Set(0, true);

	// Led del stick P0,22
	Chip_IOCON_PinMux(LPC_IOCON, LEDStick_port, LEDStick_pin, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LEDStick_port, LEDStick_pin);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void) {

	// Variables
	queue_led = xQueueCreate(1,sizeof(int));

	// Inicializacion
	prvSetupHardware();

	// Tareas
	xTaskCreate(vBTask, "vBTask",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate(vLEDTask, "vLEDTask",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}
