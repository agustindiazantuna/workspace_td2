/*
====================================================================================================================
 Name        : eeprom_lpc1769.c
 Author      : Agustin Díaz Antuña
 Contacto	 : * audiodplab@frba.utn.edu.ar
 	 	 	   * Asunto: Workspace TD2 LPC1769
 Version     : 1.0
 Copyright   : $(copyright)
 Date		 : 20/09/16
 Description : Practica realizada en clase.
 	 	 	   Escribe la eeprom del stick LPC1769 mediante el periferico I2C y
 	 	 	   luego la lee para saber si se escribio correctamente.
 Board		 : --
 Peripherals : GPIO (led) - I2S - EEPROM (del stick LPC1769)
 FreeRTOS	 : SI
====================================================================================================================
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define LED_stick					0,22
#define SDA1						0,19
#define SCL1						0,20

#define SPEED_100KHZ				100000
#define I2C_SLAVE_EEPROM_ADDR		0x50

#define LED_on						1
#define LED_off						0
#define TIMER_match0				0

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

xSemaphoreHandle SEM_led;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Inicializacion
void prvSetupHardware(void)
{
	SystemCoreClockUpdate();

	// Led stick
	Chip_IOCON_PinMux(LPC_IOCON, LED_stick, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_stick);

	// I2C
	Chip_IOCON_PinMux(LPC_IOCON, SDA1, IOCON_MODE_INACT, IOCON_FUNC3);
	Chip_IOCON_PinMux(LPC_IOCON, SCL1, IOCON_MODE_INACT, IOCON_FUNC3);
	Chip_IOCON_EnableOD(LPC_IOCON, SDA1);
	Chip_IOCON_EnableOD(LPC_IOCON, SCL1);

	Chip_I2C_Init(I2C1);
	Chip_I2C_SetClockRate(I2C1, SPEED_100KHZ);
	Chip_I2C_SetMasterEventHandler(I2C1, Chip_I2C_EventHandler);

	NVIC_EnableIRQ(I2C1_IRQn);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Interrupciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void I2C1_IRQHandler(void)						// Controla la irq del periferico I2C, tiene que ir
{
	Chip_I2C_MasterStateHandler(I2C1);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Tareas
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Tarea led que parpadea para indicar que todo esta bien
void vLEDTask1(void *pvParameters)
{
	// Estado inicial
	Chip_GPIO_SetPinState(LPC_GPIO, LED_stick, 0);

	while (1)
	{
		vTaskDelay( 1000 / portTICK_RATE_MS );
		Chip_GPIO_SetPinToggle(LPC_GPIO, LED_stick);
	}
}

// Tarea que escribe la eeprom y lee el valor escrito
// El valor de la posicion de memoria depende del tamanio de la memoria, en este caso se necesitan mas de 8 bits
// para representar todas las posiciones.
void vI2CTask(void *pvParameters)
{
	uint8_t buff[5], data_read[3], i = 0;
	buff[0] = 0x0F;		// Parte alta de la posicion de memoria
	buff[1] = 0xAF;		// Parte baja de la posicion de memoria
	buff[2] = 0x5A;		// Data1 -> 0101 1010
	buff[3] = 0xFF;		// Data2 -> 1111 1111
	buff[4] = 0x0F;		// Data3 -> 0000 1111

	while (1)
	{
		Chip_I2C_MasterSend(I2C1, I2C_SLAVE_EEPROM_ADDR, buff, 5);			// Escribo todo el buffer que incluye la posicion de memoria y datos
		vTaskDelay( 1000 / portTICK_RATE_MS );								// Hay que esperar cierto tiempo hasta que se escriba (es lenta)
		Chip_I2C_MasterSend(I2C1, I2C_SLAVE_EEPROM_ADDR, buff, 2);			// Mandamos la posicion de memoria a partir de la que queremos leer
		Chip_I2C_MasterRead(I2C1, I2C_SLAVE_EEPROM_ADDR, data_read, 3);		// Leemos desde la posicion de memoria que se mando antes

		for(i=2;i<5;i++)
			data_read[i] = 0;												// Borramos el buffer una vez que ya se leyo para repetir la operacion
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void)
{
	// Variables
	vSemaphoreCreateBinary(SEM_led);

	// Inicializacion
	prvSetupHardware();

	// Tareas
	xTaskCreate(vLEDTask1, "vTaskLed1",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate(vI2CTask, "vI2CTask",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}


