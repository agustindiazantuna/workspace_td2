/*
====================================================================================================================
 Name        : i2s_mixto.c
 Author      : Agustin Díaz Antuña
 Contacto	 : * audiodplab@frba.utn.edu.ar
 	 	 	   * Asunto: Workspace TD2 LPC1769
 Version     : 1.0
 Copyright   : $(copyright)
 Date		 : 02/08/16
 Description : Proyecto realizado para probar la comunicacion con conversores externos (ADC y DAC) con el protocolo
 	 	 	   I2S de Philips. Utiliza interrupciones.
 	 	 	   Fue utilizado con un ADC PCM1802 y un DAC PCM1781, ambos de Texas Instruments.
 	 	 	   En cada interrupcion de I2S se toma una muestra y se la saca por el DAC interno del micro tambien.
 	 	 	   fs = 32 KHz
 	 	 	   bclk = fs * 64 = 2.048 MHz
 	 	 	   mclk = fs * 384 = 12.288 MHz
 	 	 	   Estas frecuencias se pueden cambiar, revisar manual del LPC1769.
 Board		 : --
 Peripherals : GPIO (led) - I2S (tx y rx) - DMA
 FreeRTOS	 : SI (queues)
====================================================================================================================
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "board.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// DAC: LPC 1769
#define AOUT				0,26

// Cambiar segun corresponda al conexionado con el conversor/periferico
// DAC: PCM 1802
#define I2SRX_CLK			0,23
#define I2SRX_WS			0,24
#define I2SRX_SDA			0,25
#define RX_MCLK				4,28

// Hay un error en algunas versiones de LPC1769 en las cuales el canal TX del periferico I2S las frecuencias no son
// exactas, no se cual es el problema, ya que el mismo codigo en otros sticks funciona bien.
// No encontre informacion en la errata sheet ni en internet.
// DAC: PCM 1781
#define I2STX_CLK			2,11
#define I2STX_WS			2,12
#define I2STX_SDA			2,13
#define TX_MCLK				4,29

// LED del stick
#define LED_stick			0,22

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

uint32_t data = 0;
uint16_t adcFlag = 0;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Inicializacion
static void InitHardware(void)
{
	// Configuro el timer del micro en 120MHz						NO ANDA!
	Chip_SetupXtalClocking();
	Chip_SYSCTL_SetFLASHAccess(FLASHTIM_120MHZ_CPU);

	SystemCoreClockUpdate();

	// Led del stick
	Chip_IOCON_PinMux(LPC_IOCON, LED_stick, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_stick);

	// Inicializacion del I2S
	// I2S RX de P0
	Chip_IOCON_PinMux(LPC_IOCON, I2SRX_CLK, MD_PLN, IOCON_FUNC2);
	Chip_IOCON_PinMux(LPC_IOCON, I2SRX_SDA, MD_PLN, IOCON_FUNC2);
	Chip_IOCON_PinMux(LPC_IOCON, I2SRX_WS, MD_PLN, IOCON_FUNC2);
	Chip_IOCON_PinMux(LPC_IOCON, RX_MCLK, MD_PLN, IOCON_FUNC1);

	// I2S TX de P0
	Chip_IOCON_PinMux(LPC_IOCON, I2STX_CLK, MD_PLN, IOCON_FUNC3);
	Chip_IOCON_PinMux(LPC_IOCON, I2STX_SDA, MD_PLN, IOCON_FUNC3);
	Chip_IOCON_PinMux(LPC_IOCON, I2STX_WS, MD_PLN, IOCON_FUNC3);
	Chip_IOCON_PinMux(LPC_IOCON, TX_MCLK, MD_PLN, IOCON_FUNC1);

	// Configuro I2S_TX usando la estructura I2S_AUDIO_FORMAT_T y modifico la función
	I2S_AUDIO_FORMAT_T audio_Confg;
	audio_Confg.SampleRate = 32000;
	audio_Confg.ChannelNumber = 2;			// 1 mono 2 stereo
	audio_Confg.WordWidth = 32;				// Word Len

	// Configuro el clk del periférico para que trabaje a 96MHz
	Chip_Clock_SetPCLKDiv(SYSCTL_PCLK_I2S, SYSCTL_CLKDIV_1);

	Chip_I2S_Init(LPC_I2S);

	Chip_I2S_RxStop(LPC_I2S);
	Chip_I2S_TxStop(LPC_I2S);
	Chip_I2S_EnableMute(LPC_I2S);

	// Configuro modo RX
	Chip_I2S_RxConfig(LPC_I2S, &audio_Confg);
	Chip_I2S_TxConfig(LPC_I2S, &audio_Confg);

	Chip_I2S_RxStart(LPC_I2S);
	Chip_I2S_TxStart(LPC_I2S);
	Chip_I2S_DisableMute(LPC_I2S);

	Chip_I2S_Int_TxCmd(LPC_I2S, DISABLE, 1);
	Chip_I2S_Int_RxCmd(LPC_I2S, ENABLE, 1);

	NVIC_EnableIRQ(I2S_IRQn);

	// Inicializacion del DAC
	Chip_DAC_Init(LPC_DAC);
	Chip_IOCON_PinMux(LPC_IOCON, AOUT, MD_PLN, IOCON_FUNC2);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Interrupciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// IRQ I2S
void I2S_IRQHandler(void)
{
	if(Chip_I2S_GetRxLevel(LPC_I2S))
	{
		data = Chip_I2S_Receive(LPC_I2S);		// Obtengo el dato recibido
		adcFlag = 1;							// Flag que indica que hay un nuevo dato
	}

	Chip_I2S_Send(LPC_I2S, data);				// Saco el dato por I2S Tx hacia una DAC externo si lo hay
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void)
{
	uint32_t aux = 0, count = 0;

	InitHardware();

	while(1)
	{
		if(adcFlag)								// Si llego un nuevo dato lo saco por el DAC interno del LPC1769
		{
			aux = data;

			aux = aux >> 8;						// El dato esta en la parte mas significativa de los 32 bits
												// El dato esta signado, le quito el signo (ver complemento a2)
			if(aux < 16777216)					// 2^24
				aux = aux + 8388608;
			else
				aux = aux - 8388608;

			aux &= (0x00FFFFFF);

			data = aux >> 8;					// El DAC del LPC recibe un uint16, por eso desplazo

			Chip_DAC_UpdateValue(LPC_DAC, data);
			adcFlag = 0;
		}

		if(count == 500000)						// Demora rustica para ver parpadear un led que indica que todo esta bien
		{
			Chip_GPIO_SetPinToggle(LPC_GPIO, LED_stick);
			count = 0;
		}
		count ++;
	}

	/* Should never arrive here */
	return 1;
}

