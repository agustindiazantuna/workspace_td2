/*
====================================================================================================================
 Name        : buttoncounter_infotronic.c
 Author      : Agustin Díaz Antuña
 Contacto	 : * audiodplab@frba.utn.edu.ar
 	 	 	   * Asunto: Workspace TD2 LPC1769
 Version     : 1.0
 Copyright   : $(copyright)
 Date		 : 02/08/16
 Description : Practica realizada en clase.
 	 	 	   Al presionar el boton SW1 comenzamos la cuenta, con el botón SW2
 	 	 	   aumentamos el numero contado. Al volver a apretar el boton SW1
 	 	 	   un led parpadea el valor almacenado. Incluye antirrebote clasico.
 Board		 : Infotronic V2.0
 Peripherals : GPIO (button - led)
 FreeRTOS	 : SI (queues)
====================================================================================================================
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "chip.h"
#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Led
#define LEDStick		0,22
#define LED_on			1
#define LED_off			0

// Switch
#define SW1 			2,10
#define SW2				0,18

// Debounce
#define WAIT_1 			0
#define VALIDATE_1 		1
#define WAIT_0 			2
#define VALIDATE_0 		3

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

xQueueHandle queue_led;
xQueueHandle queue_tecla;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Tareas
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Tarea que unicamente realiza un antirrebote clasico. Podria haberse realizado de una forma mas sencilla con un delay
void vDEBOUNCETask(void *pvParameters)
{
	int EstadoDebounce = WAIT_1, tecla_actual = 0, tecla_anterior = 0;

	while (1)
	{
		tecla_actual = 0;
		switch(EstadoDebounce)
		{
		case WAIT_1:
			if( Chip_GPIO_GetPinState( LPC_GPIO , SW1 ) == 0 )
					tecla_actual = 1;
			if( Chip_GPIO_GetPinState( LPC_GPIO , SW2 ) == 0 )
					tecla_actual = 2;
			if( tecla_actual != 0)
				EstadoDebounce = VALIDATE_1;
			tecla_anterior = tecla_actual;
			break;
		case VALIDATE_1:
			vTaskDelay(50/portTICK_RATE_MS);

			if( Chip_GPIO_GetPinState( LPC_GPIO , SW1 ) == 0 )
					tecla_actual = 1;
			if( Chip_GPIO_GetPinState( LPC_GPIO , SW2 ) == 0 )
					tecla_actual = 2;
			if( tecla_anterior == tecla_actual)
			{
				EstadoDebounce = WAIT_0;
				xQueueSendToBack( queue_tecla, &tecla_actual, portMAX_DELAY );
			}
			else
				EstadoDebounce = WAIT_1;
			break;
		case WAIT_0:
			if( Chip_GPIO_GetPinState( LPC_GPIO , SW1 ) == 0 )
					tecla_actual = 1;
			if( Chip_GPIO_GetPinState( LPC_GPIO , SW2 ) == 0 )
					tecla_actual = 2;
			if( tecla_actual == 0 )
				EstadoDebounce = VALIDATE_0;
			break;
		case VALIDATE_0:
			vTaskDelay(50/portTICK_RATE_MS);

			if( Chip_GPIO_GetPinState( LPC_GPIO , SW1 ) == 0 )
					tecla_actual = 1;
			if( Chip_GPIO_GetPinState( LPC_GPIO , SW2 ) == 0 )
					tecla_actual = 2;
			if( 0 == tecla_actual)
				EstadoDebounce = WAIT_1;
			else
				EstadoDebounce = WAIT_0;
			break;
		default:
			break;
		}
	}
}

// Tarea encargada de comenzar, aumentar la cuenta y cargar el valor contado mediante una queue
void vSWITCHTask(void *pvParameters)
{
	int tecla = 0, contador = 0, control = 0;

	while (1)
	{
		if (  xQueueReceive(queue_tecla, &tecla, portMAX_DELAY) == 1 )			// Si se cargo el valor del boton pasado por el antirrebote
		{
			if ( tecla == 1 && control == 0 )									// Comienzo la cuenta
			{
				control = 1;
			}
			else if ( tecla == 2 && control == 1 )								// Incremento el valor
				contador += 1;
			else if ( tecla == 1 && control == 1 )								// Informo el valor contado
			{
				xQueueSendToBack(queue_led, &contador, portMAX_DELAY);
				control = 0;
				contador = 0;
			}
		}
	}
}

// Tarea que parpadea el led segun el valor informado
void vLEDTask(void *pvParameters)
{
	int dato = 0, i = 0;

	while (1)
	{
		xQueueReceive(queue_led, &dato, portMAX_DELAY);
		if ( dato )
			dato = dato*2;
		for ( i = 0 ; i < dato ; i++ )
		{
			vTaskDelay( 1000 / portTICK_RATE_MS );
			Chip_GPIO_SetPinToggle(LPC_GPIO, LEDStick);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Inicializacion de hardware
void prvSetupHardware(void)
{
	SystemCoreClockUpdate();

    // Set the LED to the state of "On"
    Board_LED_Set(0, true);

	// Led del stick P0,22
	Chip_IOCON_PinMux(LPC_IOCON, LEDStick, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LEDStick);

	// Inicializar los botones
	// No se lo hizo ya que amanecen prendidos como entrada.
	// Una buena practica es realizar ese inicializacion
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void)
{
	// Variables
	queue_led = xQueueCreate(1,sizeof(int));
	queue_tecla = xQueueCreate(1,sizeof(int));

	// Inicialización
	prvSetupHardware();

	// Tareas
	xTaskCreate(vDEBOUNCETask, "vDEBOUNCETask",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate(vSWITCHTask, "vSWITCHTask",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate(vLEDTask, "vLEDTask",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);


	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}
