/*
====================================================================================================================
 Name        : my_blinky.c
 Author      : Agustin Díaz Antuña
 Contacto	 : * audiodplab@frba.utn.edu.ar
 	 	 	   * Asunto: Workspace TD2 LPC1769
 Version     : 1.0
 Copyright   : $(copyright)
 Date		 :
 Description : Practica realizada en clase.
 	 	 	   Primer acercamiento a LPCOpen.
 	 	 	   Blinky del stick
 Board		 : --
 Peripherals : GPIO (led)
 FreeRTOS	 : NO
====================================================================================================================
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "chip.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define	SALIDA		1
#define ENTRADA		0
#define	pin_LED		22
#define	port_LED	0

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void)
{
	// Read clock settings and update SystemCoreClock variable
    SystemCoreClockUpdate();

 	// Variables
    int aux = 0;

    // Inicializacion del led
    Chip_IOCON_PinMux(LPC_IOCON, port_LED, pin_LED, MD_PLN, IOCON_FUNC0);
    Chip_GPIO_SetPinDIR(LPC_GPIO, port_LED, pin_LED, SALIDA);

    while(1)
    {
    	// Coloca en estado alto el pin del led
    	Chip_GPIO_SetPinOutHigh(LPC_GPIO, port_LED, pin_LED);
    	// Espera rustica
    	while(aux != 1000000)
    		aux++;
    	aux = 0;
    	// Coloca en estado bajo el pin del led
    	Chip_GPIO_SetPinOutLow(LPC_GPIO, port_LED, pin_LED);
    	// Espera rustica
    	while(aux != 1000000)
    		aux++;
    	aux = 0;
    }
}
