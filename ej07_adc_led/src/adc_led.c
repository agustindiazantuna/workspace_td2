/*
====================================================================================================================
 Name        : adc_led.c
 Author      : Agustin Díaz Antuña
 Contacto	 : * audiodplab@frba.utn.edu.ar
 	 	 	   * Asunto: Workspace TD2 LPC1769
 Version     : 1.0
 Copyright   : $(copyright)
 Date		 : 30/08/16
 Description : Practica realizada en clase.
 	 	 	   Utilizando el potenciometro del LPC 1769 BOARD prendemos y apagamos
 	 	 	   un led.
 	 	 	   Configurando un match del TIMER0, cada vez que el mismo interrumpe,
 	 	 	   dispara una conversion del ADC. Cuando el valor es leido, genera
 	 	 	   una interrupcion en la que leemos el valor del ADC y cargamos ese
 	 	 	   numero en una queue. Una tarea lo evaluara y prendera o apagara el
 	 	 	   led si supera o no cierto valor umbral.
 Board		 : LCP 1769 BOARD
 Peripherals : GPIO (led) - TIMER (IRQ) - ADC (IRQ) - potenciometro (LPC 1769 BOARD)
 FreeRTOS	 : SI (queue)
====================================================================================================================
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define LED_stick		0,22
#define TIMER_match0	0
#define FS_CH0			200000

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

xQueueHandle data_from_adc;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Inicializacion del hardware
void prvSetupHardware(void)
{
	SystemCoreClockUpdate();

	// Init board
    Board_Init();

	// LED stick
	Chip_IOCON_PinMux(LPC_IOCON, LED_stick, MD_PLN, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_stick);

	// Init ADC
	ADC_CLOCK_SETUP_T adc;

	Chip_ADC_Init(LPC_ADC, &adc);									// Inicializamos el periferico
	Chip_ADC_SetSampleRate(LPC_ADC, &adc, FS_CH0);					// Configuramos la frecuencia de muestreo
	Chip_ADC_EnableChannel(LPC_ADC, ADC_CH0, ENABLE);				// Habilitamos el canal asociado al potenciometro
	Chip_ADC_Int_SetChannelCmd(LPC_ADC, ADC_CH0, ENABLE);			// Habilitamos las interrupciones del canal
	Chip_ADC_SetBurstCmd(LPC_ADC, DISABLE);							// Desactivamos el modo de conversion por rafagas
	NVIC_EnableIRQ(ADC_IRQn);										// Habilitamos las interrupciones.

	// TIMER0
	Chip_TIMER_Init(LPC_TIMER0);									// Inicializamos, energizamos el periférico.
	Chip_Clock_SetPCLKDiv(SYSCTL_PCLK_TIMER0, SYSCTL_CLKDIV_1);		// Le damos una frecuencia.
	Chip_TIMER_PrescaleSet(LPC_TIMER0, 0);
																	// Configuramos el match.
	Chip_TIMER_SetMatch(LPC_TIMER0, TIMER_match0, SystemCoreClock / 10);		// 100mS * Main Clock
	Chip_TIMER_MatchEnableInt(LPC_TIMER0, TIMER_match0);
	Chip_TIMER_ResetOnMatchEnable(LPC_TIMER0, TIMER_match0);
	Chip_TIMER_StopOnMatchDisable(LPC_TIMER0, TIMER_match0);
	Chip_TIMER_ExtMatchControlSet(LPC_TIMER0, 0, TIMER_EXTMATCH_DO_NOTHING, 0);
	Chip_TIMER_Enable(LPC_TIMER0);									// Disparamos el timer.
	NVIC_EnableIRQ(TIMER0_IRQn);									// Habilitamos las interrupciones.
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Interrupciones
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// IRQ del TIMER
void TIMER0_IRQHandler(void)
{
	// Nos fijamos si fue interrupción por el match
	if(Chip_TIMER_MatchPending(LPC_TIMER0, TIMER_match0))
	{
		Chip_TIMER_ClearMatch(LPC_TIMER0, TIMER_match0);							// Borramos el match
		Chip_ADC_SetStartMode(LPC_ADC, ADC_START_NOW, ADC_TRIGGERMODE_RISING);		// Disparo ADC
	}
}

// IRQ del ADC
void ADC_IRQHandler(void)
{
	uint16_t data = 0;
	portBASE_TYPE pxHigherPriorityTaskWoken = pdFALSE;

	Chip_ADC_ReadValue(LPC_ADC, ADC_CH0, &data);									// Realizo una conversion con el ADC

	xQueueSendFromISR(data_from_adc, &data, &pxHigherPriorityTaskWoken);			// Cargo en la queue el valor leido
	portEND_SWITCHING_ISR(pxHigherPriorityTaskWoken);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Tareas
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Tarea que controla el estado del led en base al valor leido por el ADC
void vADCTask(void *pvParameters)
{
	uint16_t data = 0;

	// Estado inicial
	Chip_GPIO_SetPinState(LPC_GPIO, LED_stick, 0);

	while (1)
	{
		xQueueReceive(data_from_adc, &data, portMAX_DELAY);

		if(data > 2048)											// Si el valor leido por el ADC es mayor que 2048 prendo el led, sino lo apago
			Chip_GPIO_SetPinOutHigh(LPC_GPIO, LED_stick);
		else
			Chip_GPIO_SetPinOutLow(LPC_GPIO, LED_stick);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void)
{
	// Variables
	data_from_adc = xQueueCreate(1, sizeof(int));

	// Inicializaciones
	prvSetupHardware();

	// Tareas
	xTaskCreate(vADCTask, "vADCTask",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}
